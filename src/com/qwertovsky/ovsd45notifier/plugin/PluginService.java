package com.qwertovsky.ovsd45notifier.plugin;

import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

import org.eclipse.swt.widgets.Menu;

import com.qwertovsky.ovsd45notifier.Notifier;

public class PluginService
{
	private List<Plugin> plugins = new ArrayList<Plugin>();
	private static volatile PluginService service = null;
	private ServiceLoader<Plugin> pluginLoader = null;
	
	public static synchronized PluginService getInstance()
	{
		if(service == null)
			service = new PluginService();
		return service;
	}
	
	// -------------------------------------------
	private PluginService()
	{
		File pluginsDir = new File("plugins");
		FilenameFilter filter = new FilenameFilter()
			{
				public boolean accept(File dir, String name)
				{
					if(name.endsWith(".jar") || name.endsWith(".zip"))
						return true;
					return false;
				}
			};
		if(!pluginsDir.exists())
			return;
		File[] archives = pluginsDir.listFiles(filter);
		URL[] pluginUrls = new URL[archives.length];
		for(int i = 0; i < archives.length; i++)
		{
			try
			{
				pluginUrls[i] = archives[i].toURI().toURL();
			} catch (MalformedURLException e)
			{
				Notifier.logger.warn("Ошибка загрузки плагина: " + e.getMessage());
			}
		}
		URLClassLoader classLoader = new URLClassLoader(pluginUrls);
		pluginLoader = ServiceLoader.load(Plugin.class, classLoader);
		Iterator<Plugin> iterator = pluginLoader.iterator();
		while(iterator.hasNext())
		{
			Plugin plugin = iterator.next();
			plugin.setLogger(Notifier.logger);
			plugins.add(plugin);
		}
	}

	// -------------------------------------------
	public void createMenuItems(Menu parent)
	{
		for(Plugin plugin:plugins)
		{
			try
			{
				plugin.createMenuItem(parent);
			} catch(Exception e)
			{
				Notifier.logger.warn("Ошибка в плагине " +
						plugin.getName() + " во создания меню: "
						+ e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	// -------------------------------------------
	public boolean processServiceCall(ServiceCall serviceCall)
	{
		boolean showNotification = true;
		for(Plugin plugin:plugins)
		{
			if(!plugin.isEnabled())
				continue;
				
			try
			{
				if (!plugin.processServiceCall(serviceCall))
					showNotification = false;
			} catch (Exception e)
			{
				Notifier.logger.warn("Ошибка в плагине " + plugin.getName()
						+ " во время обработки заявки: " + e.getMessage());
				e.printStackTrace();
			}
		}
		return showNotification;
	}
	
}
