package com.qwertovsky.ovsd45notifier.mail;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.qwertovsky.ovsd45notifier.Notifier;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public class POPMailer extends Mailer
{
	public POPMailer() throws IOException
	{
		super();
	}
	//-----------------------------------------------------
	public Map<String, String> getMail()
	{
		//get mails
		Notifier.logger.trace("Создаем сессию для получения почты");
		Session session = Session.getInstance(mailProp, null);
    	Store store = null;
    	Folder inbox = null;
    	Map<String, String> subjects = new HashMap<String, String>();
		try
		{
			Notifier.logger.trace("Получаем хранилище почты");
			store = session.getStore("pop3");
			Notifier.logger.trace("Соединение с хранилищем");
			store.connect(user, password);
			Notifier.logger.trace("Получаем папку входящие");
			inbox = store.getFolder("INBOX");
			Notifier.logger.trace("Открываем папку входящие");
			inbox.open(Folder.READ_WRITE);
			Notifier.logger.trace("Количество писем");
			int allCount = inbox.getMessageCount();
			int newCount = inbox.getNewMessageCount();
			Notifier.logger.trace("Всего "+allCount);
			Notifier.logger.trace("Новых: "+newCount);
			Notifier.logger.trace("Получаем все письма");
			Message[] allMessages = new Message[0];
			allMessages = inbox.getMessages();
			
			Notifier.logger.trace("Ищем письма по фильтру");
			Message[] messages = new Message[0];

			messages = inbox.search(searchTerm, allMessages);
			
			Notifier.logger.trace("Цикл по найденным сообщениям");
			for(Message m:messages)
			{
				// get serviceCall ID from mail subject
				Notifier.logger.trace("Обработка сообщения");
				Notifier.logger.trace("Ищем ид заявки");
				Pattern scIDPattern = Pattern.compile(scIDRegex);
				Matcher scIDMatcher = scIDPattern.matcher(m.getSubject());
				String scID = null;
				if (scIDMatcher.find())
				{
					scID = scIDMatcher.group();
				}
				Notifier.logger.trace("Добавляем загаловок");
				subjects.put(scID,m.getSubject());
				Notifier.logger.trace("Отсылать ли ориинал письма");
				if("true".equals(sendOldMessage))
				{
					Notifier.logger.debug("Формируем оригинал письма");
					MimeMessage message = new MimeMessage(session);
					try
					{
						message.setContent(m.getContent(), m.getContentType());
						message.setFrom(new InternetAddress(emailFrom,"SD"));
						message.setRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
						message.setSubject(m.getSubject());
						Notifier.logger.debug("Посылаем оригинал письма");
						Transport.send(message);
					} catch (IOException e)
					{
						Notifier.logger.warn(e.getMessage());
					}
				}
				Notifier.logger.trace("Помечаем письмо для удаления");
				//remove mail from server
				m.setFlag(Flag.DELETED, true);
			}
			Notifier.logger.trace("Закрываем почту");
			inbox.close(true);
			store.close();
		} catch (NoSuchProviderException nspe)
		{
			Notifier.logger.warn(nspe.getMessage());
			
		} catch (MessagingException me)
		{
			Notifier.logger.warn(me.getMessage());
		} catch(Exception e)
		{
			e.printStackTrace();
		}
		Notifier.logger.trace("Вернуть полученные заголовки");
		return subjects;
	}
	//------------------------------------------------------
	public int send(ServiceCall sc, String subject)
	{
		//send notification
		if(sc == null)
			return -1;
		setNotificationReason(subject);
		Notifier.logger.trace("Сессия для отправки почты");
		Session session = Session.getInstance(mailProp, null);
		Notifier.logger.trace("Новое сообщение");
		MimeMessage message = new MimeMessage(session);
		message = makeMessage(message, sc, subject);
	    if (message == null)
	    {
	      Notifier.logger.warn("Неотправлено письмо по заявке " + sc.ID());
	      return -1;
	    }
	    Notifier.logger.trace("Отсылаем");
	    try
	    {
	      Transport.send(message);
	    }
	    catch (MessagingException e)
	    {
	      Notifier.logger.warn("Неотправлено письмо по заявке " + sc.ID());

	      e.printStackTrace();
	      return -1;
	    }
	    Notifier.logger.info("Отправлено письмо по заявке " + sc.ID());
	    return 0;
	}
	//---------------------------------------------------------
	public void dispose()
	  {
	    try
	    {
		    inbox.close(true);
		    store.close();
	    }
	    catch (MessagingException e)
	    {
	    	//to log
	    	e.printStackTrace();
	    }
	  }
	
	//----------------------------------------------------------
	@Override
	public void removeMail(String subject)
	{
		// nothing
		
	}
	
}

