package com.qwertovsky.ovsd45notifier.mail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.event.MessageCountAdapter;
import javax.mail.event.MessageCountEvent;
import javax.mail.internet.MimeMessage;

import com.qwertovsky.ovsd45notifier.Notifier;
import com.qwertovsky.ovsd45notifier.TrayManager;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public class IMAPMailer extends Mailer
{
	private MCAdapter mcAdapter;
	private Set<Message> messages = new HashSet<Message>();
	
	public IMAPMailer() throws FileNotFoundException, IOException, NoSuchProviderException, MessagingException, Exception
	{
		super();
		connect();
	}
	//---------------------------------------------------
	private void connect() throws NoSuchProviderException, MessagingException, Exception
	{
		Notifier.logger.trace("Создаем сессию для получения почты");
		session = Session.getDefaultInstance(mailProp);
		try
		{
			Notifier.logger.trace("Получаем хранилище почты");
			store = session.getStore("imap");
			Notifier.logger.trace("Соединение с хранилищем");
			store.connect(user, password);
			Notifier.logger.trace("Получаем папку входящие");
			inbox = store.getFolder("INBOX");
			Notifier.logger.trace("Открываем папку входящие");
			inbox.open(Folder.READ_WRITE);
			if(monitor.contains("email"))
			{
				mcAdapter = new MCAdapter();
				inbox.addMessageCountListener(mcAdapter);
			}
		} catch (NoSuchProviderException nspe)
		{
			Notifier.logger.warn(nspe.getMessage());
			throw nspe;
		} catch (MessagingException me)
		{
			if(!me.getMessage().equals("No route to host: connect")
				&& !me.getMessage().equals("Connection dropped by server?"))
				Notifier.logger.warn(me.getMessage());
			throw me;
		} catch(Exception e)
		{
			Notifier.logger.error("Некая ошибка "+e.getMessage());
			throw e;
		}
	}
	//---------------------------------------------------
	public synchronized Map<String, String> getMail()
	{
		try
	    {
			inbox.getMessageCount();
	    }
	    catch (Exception e)
	    {
	    	//to log
	    	Notifier.logger.error("Нет соединения с почтовым сервером");
	    	TrayManager.setProblemIcon(true, TrayManager.Problem.MAIL_CONNECT);
	    	//disconnect
	    	boolean needConnect = true;
	    	while(needConnect)
	    	{
	    		try
				{
					wait(5000L);
				} catch (InterruptedException e1)
				{
					notifyAll();
					return null;
				}
	    		try
				{
					if(inbox.isOpen())
						inbox.close(false);
					store.close();
					connect();
					needConnect = false;
				} catch (Exception e1)
				{
					continue;
				}
	    		
	    	}
	    	TrayManager.setProblemIcon(false, TrayManager.Problem.MAIL_CONNECT);
	    }
	    notifyAll();
	    Map<String, String> subjects = new HashMap<String, String>();
	    Notifier.logger.trace("Цикл по найденным сообщениям");
	    for(Message m:messages)
		{
			// get serviceCall ID from mail subject
			Notifier.logger.trace("Обработка сообщения");
			Notifier.logger.trace("Ищем ид заявки");
			String subject;
			try
			{
				subject = m.getSubject();
			} catch (MessagingException e)
			{
				// log stack trace
				e.printStackTrace();
				continue;
			}
			Pattern scIDPattern = Pattern.compile(scIDRegex);
			Matcher scIDMatcher = scIDPattern.matcher(subject);
			String scID = null;
			if (scIDMatcher.find())
			{
				scID = scIDMatcher.group();
			}
			Notifier.logger.trace("Добавляем загаловок");
			subjects.put(scID,subject);
		}
	    return subjects;
	}
	
	//---------------------------------------------------
	@Override
	public synchronized void removeMail(String subject)
	{
		Iterator<Message> messagesIterator = messages.iterator();
		while(messagesIterator.hasNext())
		{
			Message m = messagesIterator.next();
			try
			{
				String s = m.getSubject();
				if(s.equalsIgnoreCase(subject))
				{
					messagesIterator.remove();
					m.setFlag(Flag.DELETED, true);
				}
			} catch (MessagingException e)
			{
				e.printStackTrace();
				continue;
			}
			
		}
	}
	
	//---------------------------------------------------
	private synchronized void putMessages(Message[] messages)
	{
		for(Message message:messages)
		{
			this.messages.add(message);
		}
		notifyAll();
	}
	
	//---------------------------------------------------
	public int send(ServiceCall sc, String subject)
	{
		//send notification
		if(sc == null)
			return -1;
		setNotificationReason(subject);
		MimeMessage message = new MimeMessage(session);
		message = makeMessage(message, sc, subject);

	    if (message == null)
	    {
	    	Notifier.logger.warn("Неотправлено письмо по заявке " + sc.ID());
	    	return -1;
	    }

	    Notifier.logger.trace("Отсылаем");
	    try
	    {
	    	message.setFlag(Flag.SEEN, true);
	    	if(!inbox.isOpen())
	    		inbox.open(Folder.READ_WRITE);
	    	inbox.appendMessages(new Message[] { message });
	    }
	    catch (MessagingException e)
	    {
	    	Notifier.logger.warn("Неотправлено письмо по заявке " + sc.ID());
	    	e.printStackTrace();
	    	return -1;
	    }
	    Notifier.logger.trace("Отправлено письмо по заявке " + sc.ID());
	    return 0;
	}
	//---------------------------------------------------
	public void dispose()
	{
		try
	    {
			inbox.close(true);
			store.close();
	    }
	    catch (MessagingException e)
	    {
	    	e.printStackTrace();
	    }
	}
	//---------------------------------------------------
	private class MCAdapter extends MessageCountAdapter
	{
		public MCAdapter()
		{
			getMissedMessages();
		}
		//------------------------------------------
		public void getMissedMessages()
		{
			try
		    {
				Message[] messages = inbox.search(searchTerm);
				if (messages == null)
					return;
				putMessages(messages);
		    }
		    catch (Exception e)
		    {
		    	e.printStackTrace();
		    }
		}
		//------------------------------------------
		@Override
		public void messagesAdded(MessageCountEvent mce)
		{
			Message[] messages = mce.getMessages();
			try
			{
				messages = inbox.search(searchTerm, messages);
			} catch (MessagingException me1)
			{
				Notifier.logger.trace(me1.getMessage());
				return;
			}
			putMessages(messages);
		}
	}
}
