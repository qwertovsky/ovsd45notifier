package com.qwertovsky.ovsd45notifier.mail;

import com.qwertovsky.ovsd45notifier.Notifier;
import com.qwertovsky.ovsd45notifier.TrayManager;
import com.qwertovsky.ovsd45notifier.plugin.ApprovalVote;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.search.SearchTerm;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

public abstract class Mailer
{
	private String smtpHostName = null;
	private String smtpPort = null;
	private String popHostName = null;
	private String popPort = null;
	private String imapHostName = null;
	private String imapPort = null;
	protected String password = null;
	protected String user = null;
	protected String emailFrom = null;
	protected String emailTo = null;
	protected String scIDRegex = null;
	private String personFromRegex = null;
	private String subjectFromRegex = null;
	private String velocityTemplateFileName = null;
	protected String sendOldMessage = null;
	protected String monitor = "email";

	private Properties prop = null;
	protected Properties mailProp = null;
	protected Session session;
	protected Store store;
	protected Folder inbox;
	protected SearchTerm searchTerm = null;
	private Template template = null;

	

	private boolean ready = false;
	private boolean moreInfo = false;
	private boolean mustApprove = false;
	private boolean approved = false;
	private boolean notApproved = false;
	// private boolean answer = false; //Пользователь ответил на запрос по заявке №xxxxxx
	// private boolean rejected = false; //Отклонено решение по заявке №xxxxxx	
	// private boolean toGroup = false; //На Вашу группу назначена Заявка №xxxxxx.
	// private boolean toYou = false; //Вы назначены Исполнителем по Заявке №xxxxxx
	// private boolean inWork = false; //Заявка №xxxxxx возвращена в работу.
	// private boolean addInfo = false; //В заявке №xxxxxx изменилась информация о ходе работ
	// private boolean status = false; //Изменился статус заявки №xxxxxx от dd.MM.yyyy HH:mm.
	// private boolean toApprove = false; //Заявка №xxxxxx поступила на согласование
	
	public Mailer() throws IOException
	{
		this.prop = new Properties();
		try
		{
			FileInputStream propertiesFIS = new FileInputStream("conf/ovsd45notifier.properties");
			prop.load(propertiesFIS);
		} catch (FileNotFoundException fnfe)
		{
			Notifier.logger.warn("Не найден файл ovsd45notifier.properties");
			throw fnfe;
		} catch (IOException ioe)
		{
			Notifier.logger.warn("Невозможно прочитать ovsd45notifier.properties");
			throw ioe;
		}
		//get properties
		smtpHostName = prop.getProperty("smtpHostName");
		smtpPort = prop.getProperty("smtpPort");
		popHostName = prop.getProperty("popHostName");
		popPort = prop.getProperty("popPort");
		imapHostName = prop.getProperty("imapHostName");
		imapPort = prop.getProperty("imapPort");
		user = prop.getProperty("user");
		password = prop.getProperty("password");
		scIDRegex = prop.getProperty("scIDRegex");
		personFromRegex = prop.getProperty("personFromRegex");
		subjectFromRegex = prop.getProperty("subjectFromRegex");
		velocityTemplateFileName = prop.getProperty("velocityTemplateFileName");
		emailFrom = prop.getProperty("emailFrom");
		emailTo = prop.getProperty("emailTo");
		sendOldMessage = prop.getProperty("sendOldMessage");
		monitor = prop.getProperty("monitor");
		//set mail Properties
		mailProp = new Properties();
		mailProp.put("mail.smtp.host", smtpHostName);
		mailProp.put("mail.smtp.port", smtpPort);
		mailProp.put("mail.pop3.host", popHostName);
		mailProp.put("mail.pop3.port", popPort);
		mailProp.put("mail.imap.host", imapHostName);
		mailProp.put("mail.imap.port", imapPort);
		mailProp.put("mail.user", user);
		mailProp.put("mail.password", password);
		mailProp.put("mail.mime.charset", "utf-8");
		mailProp.put("mail.transport.protocol", "smtp");
		mailProp.put("mail.store.protocol", "pop3");
		mailProp.put("mail.pop3.disabletop", "true");
		mailProp.put("mail.pop3.connectiontimeout", "60000");
		mailProp.put("mail.pop3.timeout", "120000");
		//search criteria 
		searchTerm = new SearchTerm()
		{
			private static final long serialVersionUID = 1L;
			String personRegex = personFromRegex;
			String subjectRegex = subjectFromRegex;
			String address = emailFrom;

			Pattern subjectPattern = Pattern.compile(subjectRegex);
			Pattern personPattern = Pattern.compile(personRegex);

			public boolean match(Message m)
			{
				InternetAddress a = null;
				try
				{
					Notifier.logger.trace("Получаем информацию из сообщения");
					Notifier.logger.trace("Сообщение: " + m.getMessageNumber());
					if (m.getFlags().contains(Flag.DELETED))
						return false;
					if (m.getFrom().length > 0)
						Notifier.logger.trace("From: " + m.getFrom()[0]);
					Notifier.logger.trace("Subject: " + m.getSubject());
					Notifier.logger.trace("Получаем адрес отправителя");
					if ((m.getFrom() == null) || (m.getFrom().length == 0))
						return false;
					a = (InternetAddress) m.getFrom()[0];
					Notifier.logger.trace("Получаем отправителя");
					String person = a.getPersonal();
					Notifier.logger.trace("Получаем тему сообщения");
					String subject = m.getSubject();

					if (subject == null)
						return false;
					if ((a.getPersonal() == null)
							&& (personRegex.length() != 0))
					{
						return false;
					}
					Notifier.logger.trace("Получаем матчер темы");
					Matcher subjectMatcher = subjectPattern.matcher(subject);
					Notifier.logger.trace("Получаем матчер от кого");
					Matcher personMatcher = personPattern.matcher(person
							.toString());
					Notifier.logger.trace("Проверяем на совпадение");
					if ((personMatcher.find())
							&& (a.getAddress().equalsIgnoreCase(address))
							&& (subjectMatcher.find()))
					{
						Notifier.logger.trace("Есть совпадение");
						return true;
					}
				} catch (MessageRemovedException mre)
				{
					Notifier.logger
							.debug("Сообщение было удалено с сервера\r\n Номер сообщения: "
									+ m.getMessageNumber());
				} catch (MessagingException e)
				{
					Notifier.logger.warn("Ошибка: " + e.getMessage() + "\r\n"
							+ e.toString() + "\r\n " + "Номер сообщения: "
							+ m.getMessageNumber());
				}
				Notifier.logger.trace("нет совпадения\r\n");
				return false;
			}
		};
		//velocity init
		Velocity.setProperty("runtime.log.logsystem.class",
				"org.apache.velocity.runtime.log.Log4JLogChute");
		Velocity.setProperty("runtime.log.logsystem.log4j.logger",
				"com.qwertovsky.ovsd45notifier");
		Velocity.init();
		try
		{
			this.template = Velocity.getTemplate("conf/"
					+ velocityTemplateFileName, "utf-8");
		} catch (ResourceNotFoundException rnfe)
		{
			Notifier.logger.warn("Не найден файл  " + velocityTemplateFileName);
		} catch (ParseErrorException pee)
		{
			Notifier.logger.warn("Syntax error in template "
					+ velocityTemplateFileName + ":" + pee);
		}
	}

	public abstract int send(ServiceCall paramServiceCall, String paramString);

	protected void setNotificationReason(String subject)
	{
		if (subject == null)
			return;
		
		if (subject.contains("произведены"))
			ready = true;
		else
			ready = false;
		if (subject.contains("дополнительная информация"))
			moreInfo = true;
		else
			moreInfo = false;
		if (subject.contains("Согласование Заявки") || subject.contains("Approval task"))
			mustApprove = true;
		else
			mustApprove = false;
		if (subject.contains("утверждена"))
			approved = true;
		else
			approved = false;
		if (subject.contains("Отклонена"))
			notApproved = true;
		else
			notApproved = false;
//		if(subject.contains("ответил на запрос"))
//			answer = true;
//		else
//			answer = false;
//		if(subject.contains("Отклонено решение"))
//			rejected = true;
//		else
//			rejected = false;
//		if(subject.contains("группу назначена"))
//			toGroup = true;
//		else
//			toGroup = false;
//		if(subject.contains("назначены Исполнителем"))
//			toYou = true;
//		else
//			toYou = false;
//		if(subject.contains("возвращена в работу"))
//			inWork = true;
//		else 
//			inWork = false;
//		if(subject.contains("изменилась информация"))
//			addInfo = true;
//		else 
//			addInfo = false;
//		if(subject.contains("статус заявки"))
//			status = true;
//		else 
//			status = false;
//		if(subject.contains("поступила на согласование"))
//			toApprove = true;
//		else
//			toApprove = false;
	}

	protected MimeMessage makeMessage(MimeMessage message, ServiceCall sc,
			String subject)
	{
		String nFrom = sc.personName();
		try
		{
			Notifier.logger.trace("Адрес от кого");
			if (emailFrom.length() > 0)
			{
				InternetAddress addressFrom = new InternetAddress(emailFrom, nFrom, "utf-8");
				message.setFrom(addressFrom);
			} else
			{
				message.setFrom(new InternetAddress("."));
			}
			Notifier.logger.trace("Обратный адрес");
			String emailReplay = sc.personEmail();
			if(emailReplay != null)
			{
				InternetAddress addressFrom = new InternetAddress(emailReplay, nFrom, "utf-8");
				message.setReplyTo(new Address[]{addressFrom});
			}
			Notifier.logger.trace("Адрес кому");
			InternetAddress addressTo = new InternetAddress(this.emailTo);
			Notifier.logger.trace("Тема сообщения");
			message.setSubject(subject + " [" + sc.description() + "]", "utf-8");
			Notifier.logger.trace("Содержимое сообщения");
			message.setContent(getTextContent(sc), "text/html; charset=UTF-8");
			message.setHeader("Content-Transfer-Encoding", "8bit");
			Notifier.logger.trace("Получатель");
			message.setRecipient(Message.RecipientType.TO, addressTo);
		} catch (AddressException e)
		{
			Notifier.logger.warn("Проверьте ваш адрес");
			return null;
		} catch (MessagingException e)
		{
			Notifier.logger.warn("Ошибка отправки почты: " + e.getMessage());
			return null;
		} catch (UnsupportedEncodingException e)
		{
			Notifier.logger.warn("Проверьте адрес отправителя");
			return null;
		}
		return message;
	}

	private String getTextContent(ServiceCall sc)
	{
		try
		{
			Notifier.logger.debug("Внесение данных для шаблона");
			VelocityContext context = new VelocityContext();
			context.put("personID", sc.personID());
			context.put("personName", sc.personName());
			context.put("personUrl", sc.personUrl());
			context.put("personEmail", sc.personEmail());
			context.put("personPhone", sc.personPhones());
			context.put("personOrgID", sc.personDepartmentID());
			context.put("personOrgName", sc.personDepartment());
			context.put("personOrgUrl", sc.personDepartmentUrl());
			context.put("personJobTitle", sc.personJobTitle());
			context.put("personManagerName", sc.personManagerName());
			context.put("personManagerID", sc.personManagerID());
			context.put("personManagerUrl", sc.personManagerUrl());
			context.put("regDate", sc.regDate());
			context.put("impact", sc.impact());
			context.put("priority", sc.priority());
			context.put("description", sc.description());
			context.put("problem", sc.problem().replaceAll("\n", "<br \\>"));
			context.put("attachments", sc.attachments());
			context.put("assignedUserName", sc.assignedUserName());
			context.put("workHistory", sc.workHistory().replaceAll("\n", "<br \\>"));
			if (mustApprove)
			{
				context.put("mustApprove", Boolean.valueOf(true));
				context.put("approvalIniciatorName", sc.approvalIniciatorName());
				context.put("approvalIniciatorID", sc.approvalIniciatorID());
				context.put("approvalDeadline", sc.approvalDeadline());
				context.put("approvalDescription", sc.approvalDescription());
				context.put("approvalComment", sc.approvalComment());
				context.put("scUrl", TrayManager.sdWebUrl
						+ "Approval.jsp?vServicecall=" + sc.ID());
			}
			if (approved)
			{
				context.put("approved", Boolean.valueOf(true));
				context.put("approvalResult", sc.approvalResult());
				List<ApprovalVote> avs = sc.approvalVotes();
				context.put("approvalVotes", avs);
			}
			if (notApproved)
			{
				context.put("notApproved", Boolean.valueOf(true));
				context.put("approvalResult", sc.approvalResult());
				List<ApprovalVote> avs = sc.approvalVotes();
				context.put("approvalVotes", avs);
			}
			if (ready)
			{
				context.put("ready", Boolean.valueOf(true));
				context.put("scUrl", TrayManager.sdWebUrl
						+ "AcceptReject.jsp?vSc=" + sc.OID());
				context.put("scOID", sc.OID());
			}
			if (moreInfo)
			{
				context.put("moreInfo", Boolean.valueOf(true));
				context.put("scUrl", TrayManager.sdWebUrl
						+ "CreateAddInfo.jsp?vSc=" + sc.OID());
			}

			Notifier.logger.debug("Формируем письмо по шаблону");
			StringWriter mailBody = new StringWriter();
			if (template != null)
				template.merge(context, mailBody);
			mailBody.flush();
			mailBody.close();
			return mailBody.toString();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public abstract Map<String, String> getMail();
	
	public abstract void removeMail(String subject);

	public abstract void dispose();
}