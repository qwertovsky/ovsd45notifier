package com.qwertovsky.ovsd45notifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public class Notification extends Thread
{

	Shell shell;
	int curAlpha;
	private boolean fadeIn = true;
	
	private boolean ready = false;
	private boolean moreInfo = false;
	private boolean mustApprove = false;


	public Notification(final ServiceCall serviceCall)
	{
		//set reason
		String status = serviceCall.status();
		if(serviceCall.isCaller())
		{
			if(status.contains("Работы выполнены"))
			{
				ready = true;
			}
			if(status.contains("Ожидание действий пользователя"))
			{
				moreInfo = true;
			}
		}
		if(!serviceCall.isCaller())
		{
			if(status.contains("Согласование"))
			{
				mustApprove = true;
			}
		}
		//draw new notification
		shell = new Shell(Display.getDefault(), SWT.NO_TRIM | SWT.ON_TOP | SWT.NO_FOCUS);
		Monitor primMonitor = Display.getDefault().getPrimaryMonitor();
		Rectangle screen = primMonitor.getClientArea();
		shell.setBounds(0, 0, 500, 150);
		Rectangle window = shell.getBounds();
		window.x = screen.x + screen.width - window.width + 1;
		window.y = screen.y + screen.height - window.height + 1;
		shell.setBounds(window);
		//resize for attachments
		if(serviceCall.attachments()!=null)
		{
			Map<String, String> attachments = serviceCall.attachments();
			int attCount = attachments.size();
			window = shell.getBounds();
			window.height = window.height + attCount*20;
			window.y = window.y - attCount*20;
			shell.setBounds(window);
		}
		
		GridLayout gridLayout = new GridLayout(5, false);
		gridLayout.marginTop = 10;
		gridLayout.marginLeft = 10;
		gridLayout.marginRight = 10;
		gridLayout.marginBottom = 5;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 3;
		gridLayout.horizontalSpacing = 3;
		
		shell.setLayout(gridLayout);
		//Photo
		Label photoL = new Label(shell, SWT.BORDER);
		//TrustManager for site with "bad" certificate
		SSLContext sc;
		try {
			
			
			TrustManager[] tm = new TrustManager[]{new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers()
				{
					return null;
				}

				public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
				{
				}

				public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
				{
				}
			}} ;
			sc = SSLContext.getInstance("SSL");
			sc.init(null, tm, null);
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		try
		{
			Image photo = null;
			URL photoURL = new URL(serviceCall.personImageUrl());
			URLConnection photoConnection =  photoURL.openConnection();
			photo = new Image(Display.getDefault(), photoConnection.getInputStream());
			photoL.setImage(photo);
		}catch(MalformedURLException mue)
		{
			Notifier.logger.warn("Некорректный адрес фотографии");
		} catch (IOException e)
		{
			Notifier.logger.warn("Невозможно прочитать фотографию с сервера");
		}catch (SWTException swte)
		{
			Notifier.logger.warn("Создание фотографии: " + swte.getMessage());
		}
		
		
		GridData photoGD = new GridData();
		photoGD.verticalSpan = 3;
		photoGD.verticalAlignment = SWT.FILL;
		photoGD.widthHint = 90;
		photoGD.heightHint = 120;
		photoL.setLayoutData(photoGD);
		
		//FIO
		Link fioLink = new Link(shell, SWT.CENTER);
		fioLink.setTouchEnabled(false);
		fioLink.setFont(new Font(Display.getDefault(), "Tahoma", 10, SWT.BOLD));
		GridData fioGD = new GridData();
		fioGD.horizontalSpan = 2;
		fioGD.grabExcessHorizontalSpace = true;
		fioGD.horizontalAlignment = SWT.FILL;
		fioLink.setLayoutData(fioGD);
		String text =
				"<a href=\""+serviceCall.personUrl()+"\">"+serviceCall.personName()+"</a>";
		fioLink.setText(text);
		fioLink.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				org.eclipse.swt.program.Program.launch(e.text);
			}
		});
		
		//ID
		Label id = new Label(shell, SWT.RIGHT);
		id.setFont(new Font(Display.getDefault(), "Tahoma", 10, SWT.BOLD));
		GridData idGD = new GridData();
		idGD.horizontalSpan = 1;
		idGD.horizontalAlignment = SWT.END;
		id.setText(serviceCall.ID());
		id.setLayoutData(idGD);
		//check as viewed
		final Label checkL = new Label(shell, SWT.NONE);
		Image checkI = new Image(Display.getDefault(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/tasks_tsk.gif"));
		
		checkL.setImage(checkI);
		checkL.setToolTipText("Отметить как просмотренное (удалить из списка новых уведомлений)");
		GridData checkGD = new GridData();
		checkGD.horizontalSpan = 1;
		checkGD.grabExcessHorizontalSpace = false;
		checkGD.horizontalAlignment = SWT.END;
		checkL.setLayoutData(checkGD);
		checkL.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseUp(MouseEvent e)
			{
				TrayManager.dismissItem(serviceCall);
				Image check_disI = new Image(Display.getDefault(), this.getClass()
						.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/tasks_tsk_dis.gif"));
				checkL.setImage(check_disI);
				Thread.currentThread().interrupt();
				shell.dispose();
				return;
			}
		});
		
		//Contacts
		Link contactsLink = new Link(shell, SWT.CENTER);
		contactsLink.setText(serviceCall.personPhones()
				+", <a href=\"mailto:"+serviceCall.personEmail()+"\">"+serviceCall.personEmail()+" </a>");
		contactsLink.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				org.eclipse.swt.program.Program.launch(e.text);
			}
		});
		GridData contactsGD = new GridData();
		contactsGD.horizontalSpan = 4;
		contactsGD.horizontalIndent = 20;
		contactsLink.setLayoutData(contactsGD);
		//Problem
		Text problemT = new Text(shell,  SWT.V_SCROLL  | SWT.WRAP  | SWT.MULTI);
		problemT.setEditable(false);
		problemT.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NONE));
		problemT.setForeground(new Color(Display.getDefault(), 50,50,60));
		String problemText = "";
		if(serviceCall.problem().length()>0)
		{
			if(serviceCall.assignedUserName() != null)
				problemText = serviceCall.workHistory().trim();
			else 
				problemText = serviceCall.problem().trim();
		}
		else
		{
			problemText = serviceCall.description();
			if(serviceCall.assignedUserName() != null)
				problemText = problemText + "\n" + serviceCall.workHistory().trim();
			
		}
		problemT.setText(problemText);
		problemT.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				if(ready)
				{
					Program.launch(TrayManager.sdWebUrl
					+ "AcceptReject.jsp?vSc=" + serviceCall.OID());
					return;
				}
				if(mustApprove)
				{
					Program.launch(TrayManager.sdWebUrl
							+ "Approval.jsp?vServicecall=" + serviceCall.ID());
					return;
				}
				if(moreInfo)
				{
					Program.launch(TrayManager.sdWebUrl
							+ "CreateAddInfo.jsp?vSc=" + serviceCall.OID());
					return;
				}
				
				File tmp = new File("tmp.bat");
				try {
					FileOutputStream out = new FileOutputStream(tmp);
					String command = " cd \"";
					if("client".equalsIgnoreCase(TrayManager.client))
						command += "%SD_CLIENTHOME%";
					else
						command += "%SD_CLIENT2008HOME%";
			        command += "bin\"\r\n" +
	        		"sd_dataform.bat \"SC(new)\" \"ID="+serviceCall.ID()+"\"";
			        out.write(command.getBytes("CP866"));
			        out.close();
			    } catch (IOException ioe)
			    {
			    	return;
			    }
				
				Program program = org.eclipse.swt.program.Program.findProgram(".bat");
				program.execute("tmp.bat");
			}
		});
		GridData problemGD = new GridData();
		problemGD.verticalIndent = 5;
		problemGD.horizontalSpan = 4;
		problemGD.grabExcessVerticalSpace = false;
		problemGD.heightHint = 60;
		problemGD.grabExcessHorizontalSpace = true;
		problemGD.horizontalAlignment = SWT.FILL;
		problemGD.verticalAlignment = SWT.FILL;
		problemT.setLayoutData(problemGD);
		
		//Attachments
		if(serviceCall.attachments()!=null)
		{
			Map<String, String> attachments = serviceCall.attachments();
			int attCount = attachments.size();
			
			Text attLabelT = new Text(shell, SWT.RIGHT);
			attLabelT.setText("Вложение:");
			attLabelT.setEditable(false);
			attLabelT.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.BOLD));
			GridData attLabelGD = new GridData();
			attLabelGD.verticalSpan = attCount;
			attLabelGD.verticalAlignment = SWT.BEGINNING;
			attLabelT.setLayoutData(attLabelGD);
			for(Entry<String, String> attachment:attachments.entrySet())
			{
				Label attImageL = new Label(shell, SWT.NONE);

				Link attLink = new Link(shell,SWT.NONE);
				GridData attGD = new GridData();
				attGD.grabExcessHorizontalSpace = true;
				attGD.horizontalSpan = 3;
				attGD.horizontalAlignment = SWT.FILL;
				attLink.setLayoutData(attGD);
				attLink.setText("<a href=\""+attachment.getValue()+"\">"
						+attachment.getKey()+"</a>");
				//find program
				String ext = attachment.getValue();
				int startIndex = ext.indexOf('=')+1;
				int endIndex = ext.indexOf('&');
				final String fileExt = new String(attachment.getValue().substring(startIndex, endIndex));
				final String fileName = "sd" + serviceCall.ID() + "_"
					+ attachment.getKey().replaceFirst(" ","");
				Program program = null;
				Image attI = null;
				if(fileExt.length()!=0)
				{
					program = Program.findProgram(fileExt);
					
				}
				if(program==null)
					;
				else
				{
					ImageData imageD = program.getImageData();
					if(imageD!=null)
					{
						attI = new Image(Display.getDefault(),imageD);
						attImageL.setImage(attI);
					}
				}
				final Program p = program;
				attLink.addSelectionListener(new SelectionAdapter()
				{
					public void widgetSelected(SelectionEvent event)
					{
						//save file
						File tempDir = new File("temp");
						tempDir.mkdir();
						tempDir.deleteOnExit();
						File tempFile = null;
						try
						{
							tempFile = File.createTempFile(fileName + "_", fileExt, tempDir);
							tempFile.deleteOnExit();
							InputStream is = new URL(event.text).openConnection().getInputStream();
							OutputStream os = new FileOutputStream(tempFile);
							try
							{
								byte[] buf = new byte[4096];
								int len;
								while((len = is.read(buf)) != -1)
								{
									os.write(buf, 0, len);
								}
							}
							catch(IOException ex)
							{
								throw ex;
							}
							finally
							{
								os.close();
							}
							if(p!=null)
								p.execute(tempFile.getPath());
							else 
								Program.launch(tempFile.getPath());
						} catch (IOException ioe)
						{
							if(p!=null)
								p.execute(event.text);
							else 
								Program.launch(event.text);
						}
						
					}
				});
			}
		}
		
		shell.setBackgroundMode(SWT.INHERIT_FORCE);
		// shell gradient background color - top
		
		
	    Color fgGradient = new Color(Display.getDefault()
	    		, TrayManager.fgGradient[0], TrayManager.fgGradient[1], TrayManager.fgGradient[2]);
	    // shell gradient background color - bottom    
	    Color bgGradient = new Color(Display.getDefault()
	    		, TrayManager.bgGradient[0], TrayManager.bgGradient[1], TrayManager.bgGradient[2]);
	    // shell border color
	    Color borderColor  = null;
	    if(serviceCall.isAssigned())
	    	borderColor = new Color(Display.getDefault(), 255, 0, 0);
	    else
	    	borderColor = new Color(Display.getDefault()
	    		, TrayManager.borderColor[0], TrayManager.borderColor[1], TrayManager.borderColor[2]);
	    Rectangle rect = shell.getClientArea();
	    Image newImage = new Image(Display.getDefault(), rect.width, rect.height);
        // create a GC object we can use to draw with
        GC gc = new GC(newImage);

        // fill background
        gc.setForeground(fgGradient);
        gc.setBackground(bgGradient);
        gc.fillGradientRectangle(rect.x, rect.y, rect.width, rect.height, true);

        // draw shell edge
        gc.setLineWidth(2);
        gc.setForeground(borderColor);
        gc.drawRectangle(rect.x + 1, rect.y + 1, rect.width - 2, rect.height - 2);
        gc.dispose();

        // now set the background image on the shell
        shell.setBackgroundImage(newImage);
        
        
		shell.setAlpha(0);
		shell.setVisible(true);
		
		MouseMoveListener mml = new MouseMoveListener()
		{
			public void mouseMove(MouseEvent e)
			{
				if(shell.getAlpha()<200)
					//vjsstanavlivaem alpha tol'ko esli zametno maloe znachenie
					//esli vosstanavlivat' vsegda okno budet mercat'
					shell.setAlpha(255);
				//set to fade in for 3 seconds on top
				setFadeIn();
				if("auto".equals(TrayManager.checkViewed))
					TrayManager.dismissItem(serviceCall);
			}
		};
		//add listener to all elements that can listen mouse move
		shell.addMouseMoveListener(mml);
		fioLink.addMouseMoveListener(mml);
		contactsLink.addMouseMoveListener(mml);
		problemT.addMouseMoveListener(mml);
		photoL.addMouseMoveListener(mml);
		
	}
	//-----------------------------------------------------
	@Override
	public void run()
	{
		//fade in and fade out
		while (true)
		{
			if(isInterrupted())
				break;
			//get alpha
			Display.getDefault().syncExec(new Runnable()
			{
				public void run()
				{
					if(shell.isDisposed())
						return;
					curAlpha = shell.getAlpha();
				}
			});
			if (fadeIn)
				curAlpha += 8;
			else
				curAlpha -= 8;
			if (curAlpha > 255)
			{
				//window complete fade in
				curAlpha = 255;
				//after pause do fade out
				fadeIn = false;
				try
				{
					Thread.sleep(TrayManager.notificationShowTime);
				} catch (InterruptedException e)
				{
					// do dispose shell
					break;
				}
				
				
			}
			if(curAlpha<0)
				//do dispose shell
				break;
			//set alpha
			Display.getDefault().syncExec(new Runnable()
			{
				
				public void run()
				{
					if(shell.isDisposed())
						return;
					shell.setAlpha(curAlpha);
					
				}
			});
			
			//do it slowly
			try
			{
				Thread.sleep(TrayManager.notificationFadeInOutTime/32);
			} catch (InterruptedException e)
			{
				// do dispose shell
				break;
			}
		}
		
		//dispose window
		Display.getDefault().asyncExec(new Runnable()
		{
			public void run()
			{
				if(shell.isDisposed())
					return;
				shell.dispose();
				
			}
		});
	}

	// -------------------------------------------
	public void setFadeIn()
	{
		fadeIn = true;
	}

}
