package com.qwertovsky.ovsd45notifier;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;

import com.qwertovsky.ovsd45notifier.plugin.PluginService;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;
import com.qwertovsky.ovsd45notifier.sd.ServiceCallImpl;
import com.qwertovsky.ovsd45notifier.sd.Session;

public class TrayManager
{
	private Display display = null;
	private static Shell mainShell = null;
	private static NoticeManager noticeManager = null;
	private static List<ServiceCall> messages = new ArrayList<ServiceCall>();
	private static List<Problem> problems = new ArrayList<Problem>();
	private static boolean flash = false;
	private static TrayItem trayItem = null;
	private static Menu menu = null;
	private static Image icon1 = null;
	private static Image icon2 = null;
	private static Image iconWarn = null;
	public static String client = "client2008";
	public static String personImageUrl = null;
	public static String personUrl = null;
	public static String personOrgUrl = null;
	public static String sdWebUrl = null;
	public static String dateFormat = null;
	public static int[] fgGradient = {240, 240, 240};
	public static int[] bgGradient = {200, 200, 200};
	public static int[] borderColor = {150, 150, 150};
	public static long notificationFadeInOutTime = 1500;
	public static long notificationShowTime = 4000;
	public static String checkViewed = "auto";
	private static boolean bShowNotifications = true;
	
	protected static boolean bEmailMonitor = true;
	protected static boolean bViewMonitor = false;
	
	private Shell sendInfoShell;
	private Text numberT;

	public void initialize(Shell shell)
	{
		final Properties prop = new Properties();
		try
		{
			FileInputStream propertiesFIS;
			propertiesFIS = new FileInputStream("conf/ovsd45notifier.properties");
			prop.load(propertiesFIS);
			personImageUrl = prop.getProperty("personImageUrl");
			personUrl = prop.getProperty("personUrl");
			personOrgUrl = prop.getProperty("personOrgUrl");
			sdWebUrl = prop.getProperty("sdWebUrl");
			dateFormat = prop.getProperty("dateFormat");
			String fgGradient = prop.getProperty("fgGradient");
			String bgGradient = prop.getProperty("bgGradient");
			String borderColor = prop.getProperty("borderColor");
			String notificationFadeInOutTime = prop.getProperty("notificationFadeInOutTime");
			String notificationShowTime = prop.getProperty("notificationShowTime");
			client = prop.getProperty("client");
			checkViewed = prop.getProperty("checkViewed");
			String sShowNotifications = prop.getProperty("showNotifications");
			if(sShowNotifications != null && sShowNotifications.equalsIgnoreCase("true"))
				bShowNotifications = true;
			else
				bShowNotifications = false;
			String monitor = "email";
			monitor = prop.getProperty("monitor");
			if(monitor != null && monitor.contains("email"))
			{
				bEmailMonitor = true;
			}
			else
				bEmailMonitor = false;
			if(monitor != null && monitor.contains("view"))
			{
				bViewMonitor = true;
			}
			else
				bViewMonitor = false;
			
			try
			{
				String[] fgColor = fgGradient.split(" ");
				TrayManager.fgGradient[0] = Integer.valueOf(fgColor[0]);
				TrayManager.fgGradient[1] = Integer.valueOf(fgColor[1]);
				TrayManager.fgGradient[2] = Integer.valueOf(fgColor[2]);
				String[] bgColor = bgGradient.split(" ");
				TrayManager.bgGradient[0] = Integer.valueOf(bgColor[0]);
				TrayManager.bgGradient[1] = Integer.valueOf(bgColor[1]);
				TrayManager.bgGradient[2] = Integer.valueOf(bgColor[2]);
				String[] borderCol = borderColor.split(" ");
				TrayManager.borderColor[0] = Integer.valueOf(borderCol[0]);
				TrayManager.borderColor[1] = Integer.valueOf(borderCol[1]);
				TrayManager.borderColor[2] = Integer.valueOf(borderCol[2]);
				
				TrayManager.notificationFadeInOutTime = Long.valueOf(notificationFadeInOutTime);
				TrayManager.notificationShowTime = Long.valueOf(notificationShowTime);
			}catch(NullPointerException npe)
			{
				Notifier.logger.warn("Не указаны цвета уведомления");
			}
			catch(NumberFormatException nfe)
			{
				Notifier.logger.warn("Не правильно указаны цвета уведомления");
			}
		}
		catch(FileNotFoundException fnfe)
		{
			Notifier.logger.warn("Не найден файл ovsd45notifier.properties");
		}
		catch (IOException ioe)
		{
			Notifier.logger.warn("Невозможно прочитать файл ovsd45notifier.properties");
		}	
		
		mainShell = shell;
		display = mainShell.getDisplay();

		noticeManager = new NoticeManager();

		icon1 = new Image(display, this.getClass().getResourceAsStream(
				"/com/qwertovsky/ovsd45notifier/resources/10.png"));
		icon2 = new Image(display, this.getClass().getResourceAsStream(
				"/com/qwertovsky/ovsd45notifier/resources/11.png"));
		iconWarn = new Image(display, this.getClass().getResourceAsStream(
			"/com/qwertovsky/ovsd45notifier/resources/10_warn.png"));
		
		Tray tray = display.getSystemTray();
		if (tray != null)
		{

			trayItem = new TrayItem(tray, SWT.NONE);
			trayItem.setImage(icon1);
			trayItem.setToolTipText("OV SD 4.5 Notifier");
			trayItem.setVisible(true);

			menu = new Menu(mainShell, SWT.POP_UP);
			MenuItem miShow = new MenuItem(menu, SWT.PUSH);
			miShow.setText("Показать новые заявки");
			miShow.setEnabled(false);
			miShow.addSelectionListener(new SelectionAdapter()
			{
				@Override
				public void widgetSelected(SelectionEvent e)
				{
					TrayManager.showNewItems();

				}
			});
			MenuItem miDismissAll = new MenuItem(menu, SWT.PUSH);
			miDismissAll.setText("Освободить все");
			miDismissAll.setEnabled(false);
			miDismissAll.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent e)
				{
					dismissAll();
				}
			});
			
			//send information menu
			MenuItem miSend = new MenuItem(menu, SWT.PUSH);
			miSend.setText("Выслать информацию по заявке...");
			miSend.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent e)
				{
					//show form
					sendInfoShell = new Shell(Display.getDefault(), SWT.ON_TOP);
					GridLayout gridLayout = new GridLayout(3, false);
					gridLayout.marginTop = 10;
					gridLayout.marginLeft = 10;
					gridLayout.marginRight = 10;
					gridLayout.marginBottom = 10;
					gridLayout.marginWidth = 0;
					gridLayout.marginHeight = 0;
					gridLayout.verticalSpacing = 5;
					gridLayout.horizontalSpacing = 5;
					sendInfoShell.setLayout(gridLayout);
					
					//label
					Label textL = new Label(sendInfoShell, SWT.NONE);
					textL.setText("Укажите номер заявки");
					GridData textGD = new GridData();
					textGD.horizontalSpan = 3;
					textGD.verticalAlignment = SWT.BEGINNING;
					textL.setLayoutData(textGD);
					
					//number
					numberT = new Text(sendInfoShell, SWT.BORDER);
					GridData numberGD = new GridData();
					numberGD.grabExcessHorizontalSpace = true;
					numberT.setLayoutData(numberGD);
					numberT.addKeyListener(new KeyAdapter()
					{
						
						@Override
						public void keyReleased(KeyEvent ke)
						{
							if(ke.keyCode == 13 || ke.keyCode == SWT.KEYPAD_CR)
							{
								sendInfo();
							}
							
						}
					});
					
					//button for send
					Button sendB = new Button(sendInfoShell, SWT.NONE);
					sendB.setText("Послать информацию");
					GridData buttonGD = new GridData();
					sendB.setLayoutData(buttonGD);
					sendB.addSelectionListener(new SelectionAdapter()
					{
						public void widgetSelected(SelectionEvent ev)
						{
							sendInfo();
						}
					});
					
					//Cancel button
					Button cancelB = new Button(sendInfoShell, SWT.NONE);
					cancelB.setText("Отмена");
					cancelB.addSelectionListener(new SelectionAdapter()
					{
						public void widgetSelected(SelectionEvent ev)
						{
							sendInfoShell.dispose();
						}
					});
					
					sendInfoShell.pack(true);
					Rectangle window = sendInfoShell.getBounds();
					Monitor primMonitor = Display.getDefault().getPrimaryMonitor();
					Rectangle screen = primMonitor.getClientArea();
					window.x = screen.x + screen.width - window.width;
					window.y = screen.y + screen.height - window.height;
					sendInfoShell.setBounds(window);
					sendInfoShell.open();
				}
			});
			
			//options menu
			Menu mOptions = new Menu(menu);
			MenuItem miOptions = new MenuItem(menu, SWT.CASCADE);
			miOptions.setText("Настройки");
			miOptions.setMenu(mOptions);
			//show notifications
			final MenuItem miShowNotification = new MenuItem(mOptions, SWT.CHECK);
			miShowNotification.setText("Показывать уведомления");
			miShowNotification.setSelection(bShowNotifications);
			miShowNotification.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent e)
				{
					if(miShowNotification.getSelection())
						bShowNotifications = true;
					else
						bShowNotifications = false;
					try
					{
						PropertiesConfiguration config = new PropertiesConfiguration("conf/ovsd45notifier.properties");
						config.setProperty("showNotifications", bShowNotifications);
						config.save();
					} catch (ConfigurationException ce)
					{
						Notifier.logger.error("Ошибка сохранения настроек");
						ce.printStackTrace();
						MessageBox mb = new MessageBox(mainShell, SWT.ICON_ERROR);
						mb.setText("Ошибка");
						mb.setMessage("Не удалось сохранить настройки в файл");
						mb.open();
					}
				}
			});
			//monitors
			Menu mMonitors = new Menu(mOptions);
			MenuItem miMonitors = new MenuItem(mOptions, SWT.CASCADE);
			miMonitors.setMenu(mMonitors);
			miMonitors.setText("Мониторы");
			final MenuItem miEmailMonitor = new MenuItem(mMonitors, SWT.CHECK);
			miEmailMonitor.setText("Почты");
			miEmailMonitor.setSelection(bEmailMonitor);
			final MenuItem miViewMonitor = new MenuItem(mMonitors, SWT.CHECK);
			miViewMonitor.setText("Вида");
			miViewMonitor.setSelection(bViewMonitor);
			miEmailMonitor.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent e)
				{
					if(miEmailMonitor.getSelection())
					{
						bEmailMonitor = true;
						Notifier.startEmailMonitor();
					}
					else
					{
						bEmailMonitor = false;
						Notifier.stopEmailMonitor();
					}
					String[] monitor = new String[]{" "," "};
					if(bEmailMonitor)
						monitor[0] = "email";
					if(bViewMonitor)
						monitor[1] = "view";
					try
					{
						PropertiesConfiguration config = new PropertiesConfiguration("conf/ovsd45notifier.properties");
						config.setProperty("monitor", monitor);
						config.save();
					} catch (ConfigurationException ce)
					{
						Notifier.logger.error("Ошибка сохранения настроек");
						ce.printStackTrace();
						MessageBox mb = new MessageBox(mainShell, SWT.ICON_ERROR);
						mb.setText("Ошибка");
						mb.setMessage("Не удалось сохранить настройки в файл");
						mb.open();
					}
				}
			});
			miViewMonitor.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent e)
				{
					if(miViewMonitor.getSelection())
					{
						bViewMonitor = true;
						Notifier.startViewMonitor();
					}
					else
					{
						bViewMonitor = false;
						Notifier.stopViewMonitor();
					}
					String[] monitor = new String[]{" "," "};
					if(bEmailMonitor)
						monitor[0] = "email";
					if(bViewMonitor)
						monitor[1] = "view";
					try
					{
						PropertiesConfiguration config = new PropertiesConfiguration("conf/ovsd45notifier.properties");
						config.setProperty("monitor", monitor);
						config.save();
					} catch (ConfigurationException ce)
					{
						Notifier.logger.error("Ошибка сохранения настроек");
						ce.printStackTrace();
						MessageBox mb = new MessageBox(mainShell, SWT.ICON_ERROR);
						mb.setText("Ошибка");
						mb.setMessage("Не удалось сохранить настройки в файл");
						mb.open();
					}
				}
			});
			
			// plugins menu
			Menu mPlugins = new Menu(menu);
			MenuItem miPlugins = new MenuItem(menu, SWT.CASCADE);
			miPlugins.setText("Расширения");
			miPlugins.setMenu(mPlugins);
			PluginService.getInstance().createMenuItems(mPlugins);
			if(mPlugins.getItemCount() == 0)
				miPlugins.setEnabled(false);
			
			//exit menu
			MenuItem miExit = new MenuItem(menu, SWT.PUSH);
			miExit.setText("Выход");
			miExit.addSelectionListener(new SelectionAdapter()
			{
				public void widgetSelected(SelectionEvent e)
				{
					// exit
					trayItem.dispose();
					noticeManager.dispose();
					mainShell.dispose();
					Shell[] shells = display.getShells();
					display.dispose();
					for(Shell s:shells)
						s.dispose();
				}
			});
			menu.setDefaultItem(miShow);

			trayItem.addMenuDetectListener(new MenuDetectListener()
			{
				public void menuDetected(MenuDetectEvent e)
				{
					menu.setVisible(true);

				}
			});
			trayItem.addSelectionListener(new SelectionAdapter()
			{
				@Override
				public void widgetDefaultSelected(SelectionEvent event)
				{
					TrayManager.showNewItems();
				}
			});

		}
	}

	// ----------------------------------------------------------
	public static void flash(List<ServiceCall> serviceCalls)
	{
		// add service calls to list
		addNew(serviceCalls);
		if(trayItem.isDisposed())
			return;
		trayItem.setToolTipText(messages.size() +" новых");
		menu.getItem(0).setEnabled(true);
		menu.getItem(1).setEnabled(true);
		//show notification
		if(bShowNotifications)
			noticeManager.showNotice(serviceCalls);
		if(messages.isEmpty())
		{
			menu.getItem(0).setEnabled(false);
			return;
		}
		//start flash tray icon
		if (!flash)
		{
			flash = true;
			Thread flashThread = new Thread(new Runnable()
			{
				public void run()
				{
					while (flash)
					{
						if (mainShell.isDisposed())
						{
							return;
						}
						mainShell.getDisplay().syncExec(new Runnable()
						{
							public void run()
							{
								if ((trayItem != null)
										&& (!trayItem.isDisposed()))
									setIcon(icon1);
							}
						});
						try
						{
							Thread.sleep(350L);
						} catch (InterruptedException localInterruptedException1)
						{
							//nothing interrupt this thread
						}
						if (mainShell.isDisposed() || !flash)
						{
							return;
						}
						mainShell.getDisplay().syncExec(new Runnable()
						{
							public void run()
							{
								if ((trayItem != null)
										&& (!trayItem.isDisposed()))
									setIcon(icon2);
							}
						});
						try
						{
							Thread.sleep(450L);
						} catch (InterruptedException localInterruptedException2)
						{
							//nothing interrupt this thread
						}
					}
				}
			});
			flashThread.start();
		}

	}
	// ----------------------------------------------------------
	public static void setProblemIcon(boolean isProblem, final Problem problem)
	{
		if(problem == null)
			return;
		
		if(isProblem)
		{
			flash = false;
			try
			{
				Thread.sleep(100L);
			} catch (InterruptedException e)
			{	
			}
			if (mainShell.isDisposed())
			{
				return;
			}
			mainShell.getDisplay().syncExec(new Runnable()
			{
				public void run()
				{
					if ((trayItem != null)
							&& (!trayItem.isDisposed()))
					{
						setIcon(iconWarn);
						trayItem.setToolTipText(problem.getCause());
					}
				}
			});
			problems.add(problem);
		}
		else
		{
			//delete this problem
			Iterator<Problem> iterator = problems.iterator();
			while(iterator.hasNext())
			{
				Problem p = iterator.next();
				if(p.equals(problem))
				{
					iterator.remove();
				}
			}
			if(problems.isEmpty())
			{
				//remove problem icon and flash messages
				mainShell.getDisplay().syncExec(new Runnable()
				{
					public void run()
					{
						if ((trayItem != null)
								&& (!trayItem.isDisposed()))
							setIcon(icon1);
						flash(messages);
					}
				});
			}
			else 
			{
				//show other broblem
				final Problem p = problems.get(0);
				mainShell.getDisplay().syncExec(new Runnable()
				{
					public void run()
					{
						if ((trayItem != null)
								&& (!trayItem.isDisposed()))
						{
							setIcon(iconWarn);
							trayItem.setToolTipText(p.getCause());
						}
					}
				});
			}
		}
			
	}
	//-----------------------------------------------------------
	private static void addNew(List<ServiceCall> serviceCalls)
	{
		for(ServiceCall serviceCall:serviceCalls)
		{
			boolean newSC = true;
			for(ServiceCall message:messages)
			{
				if(message.ID().equalsIgnoreCase(serviceCall.ID()))
				{
					newSC = false;
					break;
				}
			}
			if(newSC)
				messages.add(serviceCall);
		}
		
	}

	// ----------------------------------------------------------
	
	private static void showNewItems()
	{
		if(!messages.isEmpty() 
				&& (noticeManager.allNoticeWindow == null
					|| noticeManager.allNoticeWindow.isDisposed()
					)
			)
			noticeManager.showAllNotice(messages);
		

	}
	// ----------------------------------------------------------

	public static void dismissItem(ServiceCall serviceCall)
	{
		messages.remove(serviceCall);
		trayItem.setToolTipText(messages.size() +" новых");
		if(messages.isEmpty())
		{
			flash = false;
			setIcon(icon1);
			menu.getItem(0).setEnabled(false);
			menu.getItem(1).setEnabled(false);
		}
		
	}
	//-------------------------------------------------------------
	public void dismissAll()
	{
		messages.clear();
		trayItem.setToolTipText(messages.size() +" новых");
		if(messages.isEmpty())
		{
			flash = false;
			setIcon(icon1);
			menu.getItem(0).setEnabled(false);
			menu.getItem(1).setEnabled(false);
		}
	}
	//--------------------------------------------------------------
	private synchronized static void setIcon(Image icon)
	{
		if(!flash && icon2.equals(icon))
			return;
		trayItem.setImage(icon);
	}
	//--------------------------------------------------------------
	private void sendInfo()
	{
		try
		{
			String sNumber = numberT.getText();
			if(sNumber == null || sNumber.trim().length() == 0)
			{
				MessageBox mb = new MessageBox(mainShell, SWT.ICON_ERROR);
				mb.setText("Ошибка");
				mb.setMessage("Укажите номер заявки");
				mb.open();
				return;
			}
			sNumber = sNumber.trim();
			long lNumber = Long.valueOf(sNumber);
			com.qwertovsky.ovsd45notifier.monitors.Monitor.mailer.send(
					new ServiceCallImpl(lNumber)
							, lNumber +" - "+ "Информация по заявке");
			sendInfoShell.dispose();
		}
		catch(NumberFormatException nfe)
		{
			MessageBox mb = new MessageBox(mainShell, SWT.ICON_ERROR);
			mb.setText("Ошибка");
			mb.setMessage("Неправильный формат числа");
			mb.open();
		}
		catch (Exception ex)
		{
			Notifier.logger.error("Ошибка запроса информации по заявке" +numberT.getText());
			ex.printStackTrace();
			MessageBox mb = new MessageBox(mainShell, SWT.ICON_ERROR);
			mb.setText("Ошибка");
			mb.setMessage("Не удалось послать сообщение\n"+ ex.getMessage());
			mb.open();
		}
	}
	
	//--------------------------------------------
	public enum Problem
	{
		MAIL_CONNECT("Нет соединения с почтовым сервером")
		, SD_CONNECT("Невозможно открыть сессию с сервером SD");
		
		private String cause;
		
		Problem(String cause)
		{
			this.cause = cause;
		}
		
		public String getCause()
		{
			if(Problem.SD_CONNECT.equals(this))
				return cause +" (" + Session.getServer() + ")";
			return cause;
		}
	}
}

