package com.qwertovsky.ovsd45notifier.monitors;

import java.io.Serializable;

public class ViewMonitorStatus implements Serializable
{	
	private static final long serialVersionUID = 1L;
	String status;
	String assignedUserName;
	String workHistory;
}
