package com.qwertovsky.ovsd45notifier.monitors;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;

import org.eclipse.swt.widgets.Display;

import com.qwertovsky.ovsd45notifier.Notifier;
import com.qwertovsky.ovsd45notifier.TrayManager;
import com.qwertovsky.ovsd45notifier.mail.IMAPMailer;
import com.qwertovsky.ovsd45notifier.mail.Mailer;
import com.qwertovsky.ovsd45notifier.mail.POPMailer;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public abstract  class Monitor extends Thread
{
	private boolean stop = false;
	public static Mailer mailer = null;
	
	
	
	public Monitor() throws Exception
	{
		String mailProvider = null;
		try
		{
			FileInputStream propertiesFIS;
			propertiesFIS = new FileInputStream("conf/ovsd45notifier.properties");
			Properties prop = new Properties();
			prop.load(propertiesFIS);
			mailProvider = prop.getProperty("mailProvider");
			
		}
		catch(FileNotFoundException fnfe)
		{
			Notifier.logger.warn("Не найден файл ovsd45notifier.properties");
			
		}
		catch (IOException ioe)
		{
			Notifier.logger.warn("Невозможно прочитать ovsd45notifier.properties");
			
		}
		
		try
		{
			if(mailer == null)
			{
				if(mailProvider==null || mailProvider.equalsIgnoreCase("pop"))
					mailer = new POPMailer();
				else 
					mailer = new IMAPMailer();
			}
		}catch (FileNotFoundException fnfe) {
		      throw new Exception();
	    }
	    catch (IOException ioe) {
	      throw new Exception();
	    }
	    catch (NoSuchProviderException e)
	    {
	      e.printStackTrace();
	      throw new Exception();
	    }
	    catch (MessagingException e)
	    {
	      if ((e.getNextException() instanceof ConnectException))
	        Notifier.logger.warn("Проверьте настройки соединения с почтовым сервером");
	      else if(e.getNextException() instanceof NoRouteToHostException)
	    	Notifier.logger.warn("Нет соединения с почтовым сервером"); 
	      else
	        e.printStackTrace();
	      throw new Exception();
	    }
	    catch (Exception e)
	    {
	      e.printStackTrace();
	      throw new Exception();
	    }
	}
	//---------------------------------------
	protected abstract List<ServiceCall> getServiceCalls();
	//---------------------------------------
	protected void showNotice(final ArrayList<ServiceCall> serviceCalls)
	{
		if(serviceCalls.isEmpty())
			return;
		//show notification
		Notifier.logger.trace("уведомление послать в трей");
		Display.getDefault().asyncExec(new Runnable()
		{
			@SuppressWarnings("unchecked")
			public void run()
			{
				Notifier.logger.trace("посылаем уведомление в трей");
				TrayManager.flash((List<ServiceCall>) serviceCalls.clone());
			}
		});
		//send mail
		Notifier.logger.trace("Послать сообщение");
		sendMail(serviceCalls);
	}
	//---------------------------------------
	protected abstract void sendMail(List<ServiceCall> serviceCalls);
	// --------------------------------------
	public boolean isStop()
	{
		return stop;
	}
	//---------------------------------------
	public void setStop(boolean stop)
	{
		this.stop = stop;
	}
}

