package com.qwertovsky.ovsd45notifier.monitors;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import com.hp.itsm.api.ApiSDSession;
import com.hp.itsm.api.interfaces.IServicecall;
import com.hp.itsm.api.interfaces.IServicecallHome;

import com.qwertovsky.ovsd45notifier.Notifier;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;
import com.qwertovsky.ovsd45notifier.sd.ServiceCallImpl;
import com.qwertovsky.ovsd45notifier.sd.Session;

public class ViewMonitor extends Monitor
{
	private String viewNamePattern = "Список заявок (для Специалистов)";
	private long viewMonitorInterval = 60000L;
	
	private Long viewID = null;
	private Map<String, ViewMonitorStatus> viewServiceCalls =null;
	private ArrayList<ServiceCall> notifyServiceCalls = new ArrayList<ServiceCall>();
	//------------------------------------------------
	@SuppressWarnings("unchecked")
	public ViewMonitor() throws Exception
	{
		super();
		try
		{
			FileInputStream propertiesFIS;
			propertiesFIS = new FileInputStream("conf/ovsd45notifier.properties");
			Properties prop = new Properties();
			prop.load(propertiesFIS);
			viewNamePattern = prop.getProperty("viewNamePattern");
			String sViewMonitorInterval = prop.getProperty("viewMonitorInterval");
			viewMonitorInterval = Long.valueOf(sViewMonitorInterval)*60000;
		}catch(FileNotFoundException fnfe)
		{
			Notifier.logger.warn("Не найден файл ovsd45notifier.properties");
			
		}
		catch(NumberFormatException nfe)
		{
			Notifier.logger.warn("Интервал проверки указанного стандартного вида указан неправильно");
			
		}
		catch (IOException ioe)
		{
			Notifier.logger.warn("Невозможно прочитать файл ovsd45notifier.properties");
			
		}
		
		//load viewServiceCalls
		try
		{
			FileInputStream fos = new FileInputStream("conf/viewServiceCalls.dat");
			ObjectInputStream oos = new ObjectInputStream(fos);
			viewServiceCalls = (Map<String, ViewMonitorStatus>)oos.readObject();
		} catch (FileNotFoundException e)
		{
			Notifier.logger.warn("Не найден файл viewServiceCalls.dat");
		} catch (Exception e)
		{
			Notifier.logger.warn("Невозможно прочитать файл viewServiceCalls.dat");
		}
		if(viewServiceCalls == null)
			viewServiceCalls = new HashMap<String, ViewMonitorStatus>();
		
		

	}

	// ------------------------------------------------------
	public void run()
	{
		// get all views that are defined for service call
		try
		{
			ApiSDSession session = Session.getSession();
			IServicecallHome scHome = session.getServicecallHome();
			Hashtable<?, ?> scViews = scHome.getServicecallViews();
			// get id of view
			for (Enumeration<?> e = scViews.keys(); e.hasMoreElements();)
			{
				viewID = (Long) e.nextElement();
				String viewName = (String) scViews.get(viewID);
				if (viewName.equalsIgnoreCase(viewNamePattern))
				{
					break;
				}
			}
			Session.disposeSession(session);
		}
		catch(Exception e)
		{
			Notifier.logger.error(e.getMessage());
			viewID = null;
			Notifier.logger.warn("Указанный стандартный вид не найден");
		}
		
		if (viewID == null)
			return;
		Notifier.logger.info("Запущен монитор вида");
		while (!isInterrupted())
		{
			// get all service calls of this view
			notifyServiceCalls.addAll(getServiceCalls());
			// loop through all these service calls, and show the id and
			// description.
			if(!notifyServiceCalls.isEmpty())
				showNotice(notifyServiceCalls);
			
			try
			{
				Thread.sleep(viewMonitorInterval);
			} catch (InterruptedException e)
			{
				break;
			}
		}
		//save viewServiceCalls
		try
		{
			FileOutputStream fos = new FileOutputStream("conf/viewServiceCalls.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(viewServiceCalls);
		} catch (FileNotFoundException e)
		{
			Notifier.logger.warn("Не найден файл viewServiceCalls.dat");
		} catch (IOException e)
		{
			Notifier.logger.warn("Невозможно записать в  viewServiceCalls.dat");
		}
		Notifier.logger.info("Остановлен монитор вида");
	}

	// --------------------------------------------------------

	@Override
	protected List<ServiceCall> getServiceCalls()
	{
		List<ServiceCall> notifyServiceCalls = new ArrayList<ServiceCall>();
		List<ServiceCall> serviceCalls = new ArrayList<ServiceCall>();
		Notifier.logger.info("Получение заявок из вида");
		try
		{
			ApiSDSession session = Session.getSession();
			IServicecallHome scHome = session.getServicecallHome();
			IServicecall[] serviceCallsI = scHome.findServicecall(viewID);
			for(IServicecall sc:serviceCallsI)
			{
				try
				{
					serviceCalls.add(new ServiceCallImpl(sc.getID()));
				}catch(RuntimeException re)
				{
					Notifier.logger.warn("Ошибка открытия заявки "+sc.getID());
					re.printStackTrace();
				}
			}
			Session.disposeSession(session);
			
		}
		catch(Exception e)
		{
			Notifier.logger.warn("Ошибка получения заявок из вида");
			e.printStackTrace();
			return notifyServiceCalls;
		}
		
		// remove closed
		Iterator<Entry<String, ViewMonitorStatus>> viewSCI = viewServiceCalls.entrySet().iterator();
		while(viewSCI.hasNext())
		{
			Entry<String, ViewMonitorStatus> oldSC = viewSCI.next(); 
			boolean closed = true;
			for (ServiceCall newSC : serviceCalls)
			{
				if (newSC.ID().equals(oldSC.getKey()))
					closed = false;
			}
			if (closed)
				viewSCI.remove();
		}
		// replace changed
		for (ServiceCall newSC : serviceCalls)
		{
			if (viewServiceCalls.containsKey(newSC.ID()))
			{
				ViewMonitorStatus oldStatus = viewServiceCalls.get(newSC.ID());
				if (!oldStatus.status.equals(newSC.status())
						||(oldStatus.assignedUserName!=null && !oldStatus.assignedUserName.equals(newSC.assignedUserName()))
						||(oldStatus.workHistory!=null && !oldStatus.workHistory.equals(newSC.workHistory()))
					)
				{
					// add service call to list for notification
					notifyServiceCalls.add(newSC);
					oldStatus.status = newSC.status();
					oldStatus.assignedUserName = newSC.assignedUserName();
					oldStatus.workHistory = newSC.workHistory();
				}
			}
		}
		// add new
		for (ServiceCall newSC : serviceCalls)
		{
			if (viewServiceCalls.containsKey(newSC.ID()))
				continue;
			ViewMonitorStatus ms = new ViewMonitorStatus();
			ms.assignedUserName = newSC.assignedUserName();
			ms.status = newSC.status();
			ms.workHistory = newSC.workHistory();
			viewServiceCalls.put(newSC.ID(), ms);
			notifyServiceCalls.add(newSC);
		}
		serviceCalls.clear();
		return notifyServiceCalls;
	}
	//-----------------------------------------------------
	protected void sendMail(List<ServiceCall> serviceCalls)
	{
		if(mailer==null)
			return;
		Iterator<ServiceCall> iterator = serviceCalls.iterator();
		while(iterator.hasNext())
		{
			ServiceCall serviceCall = iterator.next();
			Notifier.logger.info("Посылаем уведомление по почте");			
			int result = mailer.send(serviceCall, serviceCall.ID() +" - "+ serviceCall.status());
			if(result == -1)
				continue;
			Notifier.logger.trace("Сообщение удалено из списка для уведомления");
			iterator.remove();
		}
	}
	//-----------------------------------------------------
	

}

