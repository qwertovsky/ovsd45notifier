package com.qwertovsky.ovsd45notifier.monitors;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import com.qwertovsky.ovsd45notifier.Notifier;
import com.qwertovsky.ovsd45notifier.mail.IMAPMailer;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;
import com.qwertovsky.ovsd45notifier.sd.ServiceCallImpl;

public class EmailMonitor extends Monitor
{
	
	Map<String, String> mailSubjects = new HashMap<String, String>();
	private long emailMonitorInterval = 60000L;

	public EmailMonitor() throws Exception
	{
		super();
		
		try
		{
			FileInputStream propertiesFIS;
			propertiesFIS = new FileInputStream("conf/ovsd45notifier.properties");
			Properties prop = new Properties();
			prop.load(propertiesFIS);
			String sEmailMonitorInterval = prop.getProperty("emailMonitorInterval");
			emailMonitorInterval = Long.valueOf(sEmailMonitorInterval)*60000;
			if (mailer instanceof IMAPMailer)
		        emailMonitorInterval = 5000L;
		}
		catch(FileNotFoundException fnfe)
		{
			Notifier.logger.warn("Не найден файл ovsd45notifier.properties");
		}
		catch(NumberFormatException nfe)
		{
			Notifier.logger.warn("Интервал проверки почты указан неправильно");
		}
		catch (IOException ioe)
		{
			Notifier.logger.warn("Невозможно прочитать ovsd45notifier.properties");
		}
	}

	public void run()
	{
		Notifier.logger.info("Запущен мониторинг почты");
		while (!isInterrupted())
		{
			Notifier.logger.trace("Цикл мониторинга почты");
			// get service calls for notification
			Notifier.logger.debug("Получение заявок");
			ArrayList<ServiceCall> notifyServiceCalls = getServiceCalls();
			Notifier.logger.trace("Есть ли заявки");
			if(notifyServiceCalls != null && !notifyServiceCalls.isEmpty())
			{
				Notifier.logger.debug("Показать уведомления");
				showNotice(notifyServiceCalls);
			}
			try
			{
				Notifier.logger.trace("Пауза монитора на заданный интервал");
				Thread.sleep(emailMonitorInterval);
			} catch (InterruptedException e)
			{
				break;
			}

		}
		Notifier.logger.info("Мониторинг почты остановлен");
	}

	// -------------------------------------------------------
	@Override
	protected ArrayList<ServiceCall> getServiceCalls()
	{
		ArrayList<ServiceCall> notifyServiceCalls = new ArrayList<ServiceCall>();
		
		// get mail from Service Desk
		Notifier.logger.debug("Получение новой почты");
		Map<String, String> newSubjects = mailer.getMail();
		if(newSubjects == null || newSubjects.isEmpty())
			return null;
		mailSubjects.putAll(newSubjects);
		Notifier.logger.trace("Получена ли почта");
		Iterator<Entry<String, String>> iterator = mailSubjects.entrySet().iterator();
		while(iterator.hasNext())
		{
			Entry<String, String> subject = iterator.next();
			Notifier.logger.trace("Обработка новой почты");
			long lSCID = Long.parseLong(subject.getKey());
			try
			{	
				Notifier.logger.info("Добавляем заявку для уведомления: "+lSCID);
				notifyServiceCalls.add(new ServiceCallImpl(lSCID));
				Notifier.logger.trace("Заявка добавлена для уведомления "+lSCID);
				mailer.removeMail(subject.getValue());
			} catch (Throwable e)
			{
				Notifier.logger.warn("Ошибка открытия заявки "+lSCID);
				Notifier.logger.trace("Удаление письма");
				mailer.removeMail(subject.getValue());
				if(e instanceof NullPointerException)
				{
					iterator.remove();
					e.printStackTrace();
					Notifier.logger.info("Заявка удалена из списка для уведовления: "+lSCID);
					continue;
				}
				if(e.getMessage() == null)
				{
					e.printStackTrace();
					continue;
				}
				if(e.getMessage().contains("Не найдены записи с этим идентификатором"))
				{
					iterator.remove();
					Notifier.logger.info("Заявка удалена из списка для уведовления: "+lSCID);
					continue;
				}
				e.printStackTrace();
			}
		}

		return notifyServiceCalls;
	}
	//-------------------------------------------------------
	protected void sendMail(List<ServiceCall> serviceCalls)
	{
		for (ServiceCall serviceCall : serviceCalls)
		{
			Notifier.logger.debug("Отправляется письмо по заявке "+serviceCall.ID());
			int result = mailer.send(serviceCall, serviceCall.ID()+" - "+mailSubjects.get(serviceCall.ID()));
			if(result == -1)
				continue;
			Notifier.logger.trace("Сообщение удалено из списка для уведомления");
			mailSubjects.remove(serviceCall.ID());
			
		}
	}
	//-------------------------------------------------------
	

}

