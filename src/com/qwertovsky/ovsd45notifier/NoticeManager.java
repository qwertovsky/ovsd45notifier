package com.qwertovsky.ovsd45notifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Region;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.qwertovsky.ovsd45notifier.plugin.PluginService;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public class NoticeManager
{
	Thread newNotificationsCreate = null;
	private List<Notification> activeNotifications = new ArrayList<Notification>();
	
	Shell allNoticeWindow = null;
	Region region = new Region();
	//images
	Image background;
	Image backwardNavI;
	Image forwardNavI;
	Image backwardNavDisI;
	Image forwardNavDisI;
	Image checkI;
	Image checkDisI;
	Image closeI;
	ServiceCall serviceCall = null;

	Text positionT = null;
	Label backwardL = null;
	Label forwardL = null;
	Label checkL = null;
	Label photoL = null;
	Link fioLink = null;
	Link contactsLink = null;
	Text problemT = null;
	Text idT = null;
	Composite attC = null;
	
	int position = 1;
	int scCount = 0;

	private ArrayList<ServiceCall> serviceCalls;
	private boolean checkViewed[];
	private PluginService pluginService = null;
	
	public NoticeManager()
	{
		pluginService = PluginService.getInstance();
		
		//TrustManager for site with "bad" certificate
		SSLContext sc;
		try {
			
			
			TrustManager[] tm = new TrustManager[]{new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers()
				{
					return null;
				}

				public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
				{
				}

				public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
				{
				}
			}} ;
			sc = SSLContext.getInstance("SSL");
			sc.init(null, tm, null);
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
	}

	

	// ------------------------------------------
	public void showNotice(final List<ServiceCall> serviceCalls)
	{
		//show notification for all new service calls
		for(ServiceCall sc:serviceCalls)
		{
			// send to plugins
			if(!pluginService.processServiceCall(sc))
			{
				TrayManager.dismissItem(sc);
				continue;
			}
			showNoticeWindow(sc);
		}
	}
	//-------------------------------------------
	private void showNoticeWindow(ServiceCall serviceCall)
	{
		//show notification
		Notification notification = new Notification(serviceCall);
		notification.setName("Notification:" +serviceCall.ID());
		notification.start();
		Shell shell = notification.shell;
		// move other notifications up
        if (!activeNotifications.isEmpty())
        {
            Iterator<Notification> nIterator = activeNotifications.iterator();
            while (nIterator.hasNext())
            {
                Notification n = nIterator.next();
                if(!n.isAlive())
                {
                	nIterator.remove();
                	continue;
                }
                Shell nShell = n.shell;
                if(!nShell.isDisposed())
            	{
                	try
                	{
	                	Point curLoc = nShell.getLocation();
		                nShell.setLocation(curLoc.x, curLoc.y - shell.getClientArea().height);
		                //remove notification that moved out display client area
		                if (curLoc.y - shell.getClientArea().height < 0)
		                {
		                    nShell.dispose();
		                    n.interrupt();
		                	nIterator.remove();
		                }
                	}catch(SWTException ex)
                	{
                		//nothing
                		Notifier.logger.warn("Перемещение уведомления вверх: " + ex.getMessage());
                	}
            	}
            }
        }
        activeNotifications.add(notification);
        
	}

	// ------------------------------------------
	public void showAllNotice(final List<ServiceCall> messages)
	{
		//show all new service calls
		serviceCalls = new ArrayList<ServiceCall>(messages);
		checkViewed = new boolean[messages.size()];
		allNoticeWindow = new Shell(Display.getDefault(), SWT.NO_TRIM | SWT.ON_TOP);
		allNoticeWindow.setBounds(0, 0, 500, 180);
		//get max size (max count of attachments)
		int attCount=0;
		for(ServiceCall serviceCall:serviceCalls)
		{
			if(serviceCall.attachments()!=null)
			{
				int att = serviceCall.attachments().size();
				if(att>attCount)
					attCount=att;
			}
		}
		
		//get service call count
		scCount = serviceCalls.size();
		position = 1;
		//get first
		serviceCall = serviceCalls.get(position-1);
		if("auto".equals(TrayManager.checkViewed))
			TrayManager.dismissItem(serviceCall);
		//set size and location for window
		region = new Region();
		region.add(15, 30, 485, 15);
		region.add(getCircle(15, 15, 45));
		region.add(485, 15, 15, 15);
		region.add(getCircle(15, 485, 15));
		region.add(360, 0, 125, 30);
		region.add(getSinus(300, 0, 60, 30));
		Rectangle bottomOfShell = new Rectangle(0, 45, 500, 135+attCount*20);
		region.add(bottomOfShell);
		allNoticeWindow.setRegion(region);
		Monitor primMonitor = allNoticeWindow.getDisplay().getPrimaryMonitor();
		Rectangle screen = primMonitor.getClientArea();
		Rectangle window = allNoticeWindow.getBounds();
		window.x = screen.x + screen.width - window.width + 1;
		window.height = window.height + attCount*20;
		window.y = screen.y + screen.height - window.height + 1;
		allNoticeWindow.setBounds(window);
		
		//images
		background = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/background2.png"));
		backwardNavI = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/backward_nav.gif"));
		forwardNavI = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/forward_nav.gif"));
		backwardNavDisI = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/backward_nav_dis.gif"));
		forwardNavDisI = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/forward_nav_dis.gif"));
		checkI = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/tasks_tsk.gif"));
		checkDisI = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/tasks_tsk_dis.gif"));
		
		closeI = new Image(allNoticeWindow.getDisplay(), this.getClass()
				.getResourceAsStream("/com/qwertovsky/ovsd45notifier/resources/close.gif"));
		//background
		allNoticeWindow.setBackgroundImage(background);
		allNoticeWindow.setBackgroundMode(SWT.INHERIT_FORCE);
        allNoticeWindow.setAlpha(255);
        //check label
		checkL = new Label(allNoticeWindow, SWT.NONE);
		checkL.setImage(checkI);
		checkL.setToolTipText("Отметить как просмотренную (удалить из списка уведомлений)");
		checkL.setBounds(450, 7, 16, 16);
		GridData excludeGD = new GridData();
		excludeGD.exclude = true;
		checkL.setLayoutData(excludeGD);
		checkL.addMouseListener(new MouseAdapter()
								{
									public void mouseUp(MouseEvent me)
									{
										TrayManager.dismissItem(serviceCall);
										if(position==scCount && scCount==1)
										{
											region.dispose();
											allNoticeWindow.dispose();
											return;
										}
										serviceCalls.remove(position-1);
										scCount--;
										if(position > scCount)
											position--;
										serviceCall = serviceCalls.get(position-1);
										redrawWindow();
										return;
									}
								});
		//close label
		Label closeL = new Label(allNoticeWindow, SWT.NONE);
		closeL.setImage(closeI);
		closeL.setToolTipText("Закрыть окно");
		closeL.setBounds(470, 7, 16, 16);
		closeL.setLayoutData(excludeGD);
		closeL.addMouseListener(new MouseAdapter()
								{
									public void mouseDown(MouseEvent me)
									{
										region.dispose();
										allNoticeWindow.dispose();
										return;
									}
								});
		//backward label
		backwardL = new Label(allNoticeWindow, SWT.NONE);
		backwardL.setImage(backwardNavI);
		backwardL.setToolTipText("Предыдущее уведомление");
		backwardL.setEnabled(false);
		backwardL.setImage(backwardNavDisI);
		backwardL.setBounds(350, 7, 16, 16);
		backwardL.setLayoutData(excludeGD);
		backwardL.addMouseListener(new MouseAdapter()
									{
										public void mouseDown(MouseEvent me)
										{
											if(position==1)
												return;
											position--;
											serviceCall = serviceCalls.get(position-1);
											redrawWindow();
											return;
										}
									});
		//position text
		positionT = new Text(allNoticeWindow, SWT.CENTER);
		positionT.setForeground(new Color(allNoticeWindow.getDisplay(), 255,255,255));
		positionT.setEditable(false);
		positionT.setBounds(370, 7, 60, 16);
		positionT.setLayoutData(excludeGD);
		positionT.setText(position +" / "+ scCount);
		
		//forward label
		forwardL = new Label(allNoticeWindow, SWT.NONE);
		if(position<scCount)
			forwardL.setImage(forwardNavI);
		else 
		{
			forwardL.setImage(forwardNavDisI);
			forwardL.setEnabled(false);
		}
		forwardL.setToolTipText("Следующее уведомление");
		forwardL.setBounds(430, 7, 16, 16);
		forwardL.setLayoutData(excludeGD);
		forwardL.addMouseListener(new MouseAdapter()
									{
										public void mouseDown(MouseEvent me)
										{
											if(position==scCount)
												return;
											position++;
											serviceCall = serviceCalls.get(position-1);
											if("auto".equals(TrayManager.checkViewed))
												TrayManager.dismissItem(serviceCall);
											redrawWindow();
											return;
										}
									});
		
		GridLayout gridLayout = new GridLayout(4, false);
		gridLayout.marginTop = 40;
		gridLayout.marginLeft = 10;
		gridLayout.marginRight = 10;
		gridLayout.marginBottom = 5;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		gridLayout.verticalSpacing = 3;
		gridLayout.horizontalSpacing = 3;
		allNoticeWindow.setLayout(gridLayout);
		
		//Photo
		photoL = new Label(allNoticeWindow, SWT.BORDER);
		photoL.setAlignment(SWT.CENTER);
		GridData photoGD = new GridData();
		photoGD.verticalSpan = 3;
		photoGD.verticalAlignment = SWT.FILL;
		photoGD.widthHint = 90;
		photoGD.heightHint = 120;
		
		Image photo = null;
		try{
			URL photoURL = new URL(serviceCall.personImageUrl());
			URLConnection photoConnection =  photoURL.openConnection();
			photo = new Image(allNoticeWindow.getDisplay(), photoConnection.getInputStream());
			photoL.setImage(photo);
		}catch(MalformedURLException mue)
		{
			Notifier.logger.warn("Некорректный адрес фотографии");
		} catch (Exception e)
		{
			Notifier.logger.warn("Невозможно прочитать фотографию с сервера\n");
		}
		photoL.setLayoutData(photoGD);
		
		//FIO
		fioLink = new Link(allNoticeWindow, SWT.CENTER);
		fioLink.setFont(new Font(allNoticeWindow.getDisplay(), "Tahoma", 10, SWT.BOLD));
		String text =
			"<a href=\""+serviceCall.personUrl()+"\">"+serviceCall.personName()+"</a>";
		fioLink.setText(text);
		GridData fioGD = new GridData();
		fioGD.horizontalSpan = 2;
		fioGD.grabExcessHorizontalSpace = true;
		fioGD.horizontalAlignment = SWT.FILL;
		fioLink.setLayoutData(fioGD);
		
		fioLink.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				org.eclipse.swt.program.Program.launch(e.text);
			}
		});
		
		//ID
		idT = new Text(allNoticeWindow, SWT.RIGHT);
		idT.setText(serviceCall.ID());
		idT.setEditable(false);
		idT.setFont(new Font(allNoticeWindow.getDisplay(), "Tahoma", 10, SWT.BOLD));
		GridData idGD = new GridData();
		idGD.horizontalSpan = 1;
		idGD.horizontalAlignment = SWT.END;
		idT.setLayoutData(idGD);
		
		
		//Contacts
		contactsLink = new Link(allNoticeWindow, SWT.CENTER);
		contactsLink.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				org.eclipse.swt.program.Program.launch(e.text);
			}
		});
		GridData contactsGD = new GridData();
		contactsGD.horizontalSpan = 3;
		contactsGD.grabExcessHorizontalSpace = true;
		contactsGD.horizontalAlignment = SWT.FILL;
		contactsLink.setText(serviceCall.personPhones()
				+", <a href=\"mailto:"+serviceCall.personEmail()+"\">"+serviceCall.personEmail()+" </a>");
		contactsLink.setLayoutData(contactsGD);
		//Problem
		problemT = new Text(allNoticeWindow,  SWT.V_SCROLL  | SWT.WRAP  | SWT.MULTI);
		String problemText = "";
		if(serviceCall.problem().length()>0)
		{
			if(serviceCall.assignedUserName() != null)
				problemText = serviceCall.workHistory().trim();
			else 
				problemText = serviceCall.problem().trim();
		}
		else
		{
			problemText = serviceCall.description();
			if(serviceCall.assignedUserName() != null)
				problemText = problemText + "\n" + serviceCall.workHistory().trim();
			
		}
		problemT.setText(problemText);
		problemT.setEditable(false);
		problemT.setFont(new Font(allNoticeWindow.getDisplay(), "Tahoma", 9, SWT.NONE));
		problemT.setForeground(new Color(allNoticeWindow.getDisplay(), 50,50,60));
		GridData problemGD = new GridData();
		problemGD.verticalIndent = 5;
		problemGD.horizontalSpan = 3;
		problemGD.grabExcessVerticalSpace = false;
		problemGD.grabExcessHorizontalSpace = true;
		problemGD.horizontalAlignment = SWT.FILL;
		problemGD.verticalAlignment = SWT.FILL;
		problemGD.heightHint = 60;
		problemT.setLayoutData(problemGD);
		problemT.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				if(serviceCall.isCaller())
				{
					String status = serviceCall.status();
					if(status.contains("Работы выполнены"))
					{
						Program.launch(TrayManager.sdWebUrl
						+ "AcceptReject.jsp?vSc=" + serviceCall.OID());
						return;
					}
					if(status.contains("Согласование"))
					{
						Program.launch(TrayManager.sdWebUrl
								+ "Approval.jsp?vServicecall=" + serviceCall.ID());
						return;
					}
					if(status.contains("Ожидание действий пользователя"))
					{
						Program.launch(TrayManager.sdWebUrl
								+ "CreateAddInfo.jsp?vSc=" + serviceCall.OID());
						return;
					}
				}
				File tmp = new File("tmp.bat");
				try {
					FileOutputStream out = new FileOutputStream(tmp);

					String command = " cd \"";
					if("client".equalsIgnoreCase(TrayManager.client))
						command += "%SD_CLIENTHOME%";
					else
						command += "%SD_CLIENT2008HOME%";
			        command += "bin\"\r\n" +
	        		"sd_dataform.bat \"SC(new)\" \"ID="+serviceCall.ID()+"\"";
			        out.write(command.getBytes("CP866"));
			        out.close();
			    } catch (IOException ioe)
			    {
			    	return;
			    }
				
				Program program = org.eclipse.swt.program.Program.findProgram(".bat");
				program.execute("tmp.bat");
			}
		});
		
		
		//Attachments
		attC = new Composite(allNoticeWindow, SWT.NONE);
		GridData attCGD =  new GridData();
		attCGD.grabExcessHorizontalSpace = true;
		attCGD.grabExcessVerticalSpace = true;
		attCGD.verticalAlignment = SWT.FILL;
		attCGD.horizontalAlignment = SWT.FILL;
		attCGD.horizontalSpan = 4;
		attC.setLayoutData(attCGD);
		
		GridLayout attLayout = new GridLayout(4, false);
		attLayout.verticalSpacing = 3;
		attLayout.horizontalSpacing = 3;
		attC.setLayout(attLayout);
		if(serviceCall.attachments()!=null)
		{
			Map<String, String> attachments = serviceCall.attachments();
			int count = attachments.size();
			
			Text attLabelT = new Text(attC, SWT.RIGHT);
			attLabelT.setText("Вложение:");
			attLabelT.setEditable(false);
			attLabelT.setFont(new Font(allNoticeWindow.getDisplay(), "Tahoma", 9, SWT.BOLD));
			GridData attLabelGD = new GridData();
			attLabelGD.widthHint = 90;
			attLabelGD.verticalSpan = count;
			attLabelGD.verticalAlignment = SWT.BEGINNING;
			attLabelGD.horizontalAlignment = SWT.END;
			attLabelGD.grabExcessVerticalSpace = true;
			attLabelT.setLayoutData(attLabelGD);
			
			for(Entry<String, String> attachment:attachments.entrySet())
			{
				Label attImageL = new Label(attC, SWT.NONE);
				GridData attImageLGD = new GridData();
				attImageLGD.verticalAlignment = SWT.BEGINNING;
				attImageL.setLayoutData(attImageLGD);
				Link attLink = new Link(attC,SWT.NONE);
				GridData attGD = new GridData();
				attGD.grabExcessHorizontalSpace = true;
				attGD.grabExcessVerticalSpace = true;
				attGD.horizontalSpan = 2;
				attGD.horizontalAlignment = SWT.FILL;
				attGD.verticalAlignment = SWT.BEGINNING;
				attLink.setLayoutData(attGD);
				attLink.setText("<a href=\""+attachment.getValue()+"\">"
						+attachment.getKey()+"</a>");
				
				
				
				//find program
				String ext = attachment.getValue();
				int startIndex = ext.indexOf('=')+1;
				int endIndex = ext.indexOf('&');
				final String fileExt = attachment.getValue().substring(startIndex, endIndex);
				final String fileName = "sd" + serviceCall.ID() + "_"
				+ attachment.getKey().replaceFirst(" ","");
				Program program = null;
				Image attI = null;
				if(fileExt.length()!=0)
				{
					program = Program.findProgram(fileExt);
					
				}
				if(program==null)
					;
				else
				{
					ImageData imageD = program.getImageData();
					if(imageD!=null)
					{
						attI = new Image(allNoticeWindow.getDisplay(),imageD);
						attImageL.setImage(attI);
					}
				}
				final Program p = program;
				attLink.addSelectionListener(new SelectionAdapter()
				{
					public void widgetSelected(SelectionEvent event)
					{
						//save file
						File tempDir = new File("temp");
						tempDir.mkdir();
						tempDir.deleteOnExit();
						File tempFile = null;
						try
						{
							tempFile = File.createTempFile(fileName + "_", fileExt, tempDir);
							tempFile.deleteOnExit();
							InputStream is = new URL(event.text).openConnection().getInputStream();
							OutputStream os = new FileOutputStream(tempFile);
							try
							{
								byte[] buf = new byte[4096];
								int len;
								while((len = is.read(buf)) != -1)
								{
									os.write(buf, 0, len);
								}
							}
							catch(IOException ex)
							{
								throw ex;
							}
							finally
							{
								os.close();
							}
							if(p!=null)
								p.execute(tempFile.getPath());
							else 
								Program.launch(tempFile.getPath());
						} catch (IOException ioe)
						{
							if(p!=null)
								p.execute(event.text);
							else 
								Program.launch(event.text);
						}
						
					}
				});
			}
			
		}
		
		
		allNoticeWindow.open();
		
		
	}
	

	//----------------------------------------------------------
	private void redrawWindow()
	{
		//change window to show next service call
		//position
		positionT.setText(position +" / "+ scCount);
		
		//photo
		Image photo = null;
		try{
			URL photoURL = new URL(serviceCall.personImageUrl());
			URLConnection photoConnection =  photoURL.openConnection();
			photo = new Image(allNoticeWindow.getDisplay(), photoConnection.getInputStream());
			photoL.setImage(photo);
		}catch(MalformedURLException mue)
		{
			Notifier.logger.warn("Некорректный адрес фотографии");
		} catch (Exception e)
		{
			Notifier.logger.warn("Невозможно прочитать фотографию с сервера");
		}
		
		//navigation rows
		if(position>1)
		{
			backwardL.setImage(backwardNavI);
			backwardL.setEnabled(true);
		}
		else 
		{
			backwardL.setImage(backwardNavDisI);
			backwardL.setEnabled(false);
		}
		if(position<scCount)
		{
			forwardL.setImage(forwardNavI);
			forwardL.setEnabled(true);
		}
		else 
		{
			forwardL.setImage(forwardNavDisI);
			forwardL.setEnabled(false);
		}
		
		//check view
		if(checkViewed[position-1])
		{
			checkL.setImage(checkDisI);
		}
		else 
		{
			checkL.setImage(checkI);
		}
		
		//id
		idT.setText(serviceCall.ID());
		//fio
		String text =
			"<a href=\""+serviceCall.personUrl()+"\">"+serviceCall.personName()+"</a>";
		fioLink.setText(text);
		
		contactsLink.setText(serviceCall.personPhones()
				+", <a href=\"mailto:"+serviceCall.personEmail()+"\">"+serviceCall.personEmail()+" </a>");
		String problemText = "";
		if(serviceCall.problem().length()>0)
		{
			if(serviceCall.assignedUserName() != null)
				problemText = serviceCall.workHistory().trim();
			else 
				problemText = serviceCall.problem().trim();
		}
		else
		{
			problemText = serviceCall.description();
			if(serviceCall.assignedUserName() != null)
				problemText = problemText + "\n" + serviceCall.workHistory().trim();
			
		}
		problemT.setText(problemText);
		
		//dispose attachments

		int childrenCount = attC.getChildren().length;
		Control[] childrens = attC.getChildren();
		for(int i=0;i<childrenCount;i++)
		{
			childrens[i].dispose();
		}
		attC.pack();
		//add new attachments
		//Attachments
		if(serviceCall.attachments()!=null)
		{
			Map<String, String> attachments = serviceCall.attachments();
			int count = attachments.size();
			
			Text attLabelT = new Text(attC, SWT.RIGHT);
			attLabelT.setText("Вложение:");
			attLabelT.setEditable(false);
			attLabelT.setFont(new Font(allNoticeWindow.getDisplay(), "Tahoma", 9, SWT.BOLD));
			GridData attLabelGD = new GridData();
			attLabelGD.verticalSpan = count;
			attLabelGD.verticalAlignment = SWT.BEGINNING;
			attLabelGD.horizontalAlignment = SWT.END;
			attLabelGD.grabExcessVerticalSpace = true;
			attLabelGD.widthHint = 90;
			attLabelT.setLayoutData(attLabelGD);
			for(Entry<String, String> attachment:attachments.entrySet())
			{
				Label attImageL = new Label(attC, SWT.NONE);

				Link attLink = new Link(attC,SWT.NONE);
				GridData attGD = new GridData();
				attGD.grabExcessHorizontalSpace = true;
				attGD.grabExcessVerticalSpace = true;
				attGD.horizontalSpan = 2;
				attGD.horizontalAlignment = SWT.FILL;
				attLink.setLayoutData(attGD);
				attLink.setText("<a href=\""+attachment.getValue()+"\">"
						+attachment.getKey()+"</a>");
				//find program
				String ext = attachment.getValue();
				int startIndex = ext.indexOf('=')+1;
				int endIndex = ext.indexOf('&');
				final String fileExt = attachment.getValue().substring(startIndex, endIndex);
				final String fileName = "sd" + serviceCall.ID() + "_"
				+ attachment.getKey().replaceFirst(" ","");
				Program program = null;
				Image attI = null;
				if(fileExt.length()!=0)
				{
					program = Program.findProgram(fileExt);
					
				}
				if(program==null)
					;
				else
				{
					ImageData imageD = program.getImageData();
					if(imageD!=null)
					{
						attI = new Image(allNoticeWindow.getDisplay(),imageD);
						attImageL.setImage(attI);
					}
				}
				final Program p = program;
				attLink.addSelectionListener(new SelectionAdapter()
				{
					public void widgetSelected(SelectionEvent event)
					{
						//save file
						File tempDir = new File("temp");
						tempDir.mkdir();
						tempDir.deleteOnExit();
						File tempFile = null;
						try
						{
							tempFile = File.createTempFile(fileName + "_", fileExt, tempDir);
							tempFile.deleteOnExit();
							InputStream is = new URL(event.text).openConnection().getInputStream();
							OutputStream os = new FileOutputStream(tempFile);
							try
							{
								byte[] buf = new byte[4096];
								int len;
								while((len = is.read(buf)) != -1)
								{
									os.write(buf, 0, len);
								}
							}
							catch(IOException ex)
							{
								throw ex;
							}
							finally
							{
								os.close();
							}
							if(p!=null)
								p.execute(tempFile.getPath());
							else 
								Program.launch(tempFile.getPath());
						} catch (IOException ioe)
						{
							if(p!=null)
								p.execute(event.text);
							else 
								Program.launch(event.text);
						}
						
					}
				});
			}
		}
		attC.pack();
		
		
	}
	// ---------------------------------------------------------
	private static int[] getSinus(int x, int y, int width, int height)
	{
		int[] points = new int[width * 2 * 2 + height * 2];
		for (int i = 0; i < width; i++)
		{
			points[i * 2] = x + i;
			points[i * 2 + 1] = y
					+ height
					- (int) (Math.sin((i - width / 2) * Math.PI / 2
							/ (width / 2))
							* (height / 2) + (height / 2));
		}
		for (int i = 0; i < height; i++)
		{
			points[2 * width + i * 2] = x + width;
			points[2 * width + i * 2 + 1] = y + i;
		}
		for (int i = 0; i < width; i++)
		{
			points[2 * width + 2 * height + i * 2] = x + width - i;
			points[2 * width + 2 * height + i * 2 + 1] = y + height;
		}
		return points;
	}

	// ----------------------------------------------------------
	private static int[] getCircle(int radius, int x, int y)
	{
		int[] points = new int[360 * 2];
		for (int i = 0; i < 360; i++)
		{
			points[i * 2] = x + (int) (radius * Math.cos(Math.toRadians(i)));
			points[i * 2 + 1] = y
					+ (int) (radius * Math.sin(Math.toRadians(i)));
		}
		return points;
	}
	// ----------------------------------------------------------

	public void dispose()
	{
		if (!activeNotifications.isEmpty())
        {
            for(Notification n: activeNotifications)
            {
            	n.interrupt();
            }
			Iterator<Notification> nIterator = activeNotifications.iterator();
            while (nIterator.hasNext())
            {
            	Notification n = nIterator.next();
            	try
				{
					n.join();
				} catch (InterruptedException e)
				{
					
				}
				nIterator.remove();
            }
            
        }
		if(backwardNavI!=null)
			backwardNavI.dispose();
		if(forwardNavI!=null)forwardNavI.dispose();
		if(backwardNavDisI!=null)backwardNavDisI.dispose();
		if(forwardNavDisI!=null)forwardNavDisI.dispose();
		if(checkI!=null) checkI.dispose();
		if(checkDisI!=null) checkDisI.dispose();
		if(closeI!=null) closeI.dispose();
		if(region!=null) region.dispose();
		if(allNoticeWindow!=null)
			allNoticeWindow.dispose();
	}
	
}

