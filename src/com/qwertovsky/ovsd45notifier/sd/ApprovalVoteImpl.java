package com.qwertovsky.ovsd45notifier.sd;

import com.hp.itsm.api.interfaces.IApprovalVote;
import com.hp.itsm.api.interfaces.IPerson;
import com.qwertovsky.ovsd45notifier.TrayManager;
import com.qwertovsky.ovsd45notifier.plugin.ApprovalVote;

public class ApprovalVoteImpl implements ApprovalVote
{
	private String approverID = null;
	private String approverName = null;
	private String approverUrl = null;
	private boolean approved = false;
	private String reason = null;
	
	
	public ApprovalVoteImpl(IApprovalVote av)
	{
		if(av==null)
			return;
		if(av.getApprover()!=null)
		{
			approverID = av.getApprover().getSourceID();
			approverName = av.getApprover().getName();
			approverUrl = getPersonUrl(av.getApprover());
		}
		try
		{
			approved = av.getApproved();
			reason = av.getReason();
			if(reason == null)
				reason = "";
		}catch(Exception e) //may be exception if assigned new approval task for service call 
		{
			//do nothing
		}
	}
	// --------------------------------------------------------------
	private String getPersonUrl(IPerson person)
	{
		if(TrayManager.personUrl!=null)
			return TrayManager.personUrl.replace("[x]", person.getSourceID());
		return null;
	}
	//----------------------------------------------------------------
	public String getPersonID()
	{
		return approverID;
	}

	public String getPersonName()
	{
		return approverName;
	}

	public String getPersonUrl()
	{
		return approverUrl;
	}

	public boolean isApproved()
	{
		return approved;
	}

	public String getReason()
	{
		return reason;
	}
}

