package com.qwertovsky.ovsd45notifier.sd;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hp.ifc.util.ApiDateUtils;
import com.hp.itsm.api.ApiSDSession;
import com.hp.itsm.api.interfaces.IAccount;
import com.hp.itsm.api.interfaces.IApprovalVote;
import com.hp.itsm.api.interfaces.IAttachedItem;
import com.hp.itsm.api.interfaces.IAttachment;
import com.hp.itsm.api.interfaces.IImpact;
import com.hp.itsm.api.interfaces.IOrganization;
import com.hp.itsm.api.interfaces.IPerson;
import com.hp.itsm.api.interfaces.IPriority;
import com.hp.itsm.api.interfaces.IServicecall;
import com.hp.itsm.api.interfaces.IServicecallHome;
import com.qwertovsky.ovsd45notifier.Notifier;
import com.qwertovsky.ovsd45notifier.TrayManager;
import com.qwertovsky.ovsd45notifier.plugin.ApprovalVote;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public class ServiceCallImpl implements ServiceCall
{
	private String sPersonImageUrl = null;
	private String sPersonUrl = null;
	private String sPersonOrgUrl = null;
	private String sdWebUrl = null;
	private String dateFormat = "dd.MM.yyyy HH.mm";
	private String OID;
	private String ID;
	private String status;
	private String regDate;
	private String description;
	private String assignedUserName;
	private String workGroupName;
	private String serviceName;
	private String problem;
	private String workHistory;
	private String impact;
	private String priority;
	private String personID;
	private String personName;
	private String personImageUrl;
	private String personUrl;
	private String personEmail;
	private String personPhones;
	private String personJobTitle;
	private String personManagerName;
	private String personManagerID;
	private String personManagerUrl;
	private String personLocation;
	private String personDepartment;
	private String personDepartmentID;
	private String personDepartmentUrl;
	private String personAddress;
	private Map<String, String> attachments;
	private String approvalIniciatorName;
	private String approvalIniciatorID;
	private String approvalDescription;
	private String approvalComment;
	private String approvalDeadline;
	private String approvalResult;
	private List<ApprovalVote> approvalVotes;
	private String dataformScript;
	private String lastNote;
	private String lastEditPerson;
	
	private SimpleDateFormat sdf = null;
	private IPerson caller = null;
	private IPerson assignedUser = null;
	private IServicecall serviceCall = null;
	private IServicecallHome scHome = null;
	private boolean isCaller = false;
	private boolean isAssigned = false;
	

	//-----------------------------------------------------
	public ServiceCallImpl(long ID) throws RuntimeException
	{
		Notifier.logger.trace("Создание новой заявки");
		Notifier.logger.trace("Получение настроек");
		getProperties();
		
		//get session
		Notifier.logger.trace("Создаем сессию с сервером");
		ApiSDSession session = Session.getSession();
		if(session == null)
		{
			TrayManager.setProblemIcon(true, TrayManager.Problem.SD_CONNECT);
			boolean needConnect = true;
			while(needConnect)
			{
				try
				{
					Thread.sleep(10000L);
				} catch (InterruptedException e1)
				{
					return;
				}
	    		
    			session = Session.getSession();
    			if(session == null)
    				continue;
    			else 
    				needConnect = false;
			}
			TrayManager.setProblemIcon(false, TrayManager.Problem.SD_CONNECT);
		}
		
		IAccount account = session.getCurrentAccount();
		//get data
		// Get the home of the servicecall entity
		Notifier.logger.trace("get home");
		scHome = session.getServicecallHome();
		// Open the servicecall with the functional id provided.
		// The functional ID of a service call is the number that
		// identifies it to the external world.
		// This is a database call that can fail for that reason and
		// because no service call with that functional ID exists.
		// Make sure to catch the exception.
		Notifier.logger.trace("Получение заявки");
		serviceCall = scHome.openServicecall(ID);
		Notifier.logger.trace("Получение заявителя");
		caller = serviceCall.getCaller();
		if(caller.getAccount() != null && account.getOID().equals(caller.getAccount().getOID()))
		{
			isCaller = true;
		}
		
		if(serviceCall.getAssignment().getAssigneePerson()!=null)
		{
			assignedUser = serviceCall.getAssignment().getAssigneePerson();
			
			if(assignedUser != null 
					&& assignedUser.getAccount() != null
					&& account.getOID().equals(assignedUser.getAccount().getOID()))
			{
				isAssigned = true;
			}
		}
		
		Notifier.logger.trace("Получение остальных данных по заявке");
		this.ID = getID();
		OID = getOID();
		status = getStatus();
		regDate = getRegDate();
		description = getDescription(); //null
		if(isCaller && getPersonId(assignedUser) != null)
		{
			personID = getPersonId(assignedUser); //null
			personName = getPersonName(assignedUser); //null
			personImageUrl = getPersonImageUrl(assignedUser); //null
			personUrl = getPersonUrl(assignedUser); //null
			personEmail = getPersonEmail(assignedUser); //null
			personPhones = getPersonPhones(assignedUser); //null
			personJobTitle = getPersonJobTitle(assignedUser); //null
			personManagerName = getPersonManagerName(assignedUser); 
			personManagerID = getPersonManagerID(assignedUser);
			personManagerUrl = getPersonManagerUrl(assignedUser); //null
			personLocation = getPersonLocation(assignedUser); //null
			personDepartment = getPersonDepartment(assignedUser); //null
			personDepartmentID = getPersonDepartmentId(assignedUser); //null
			personDepartmentUrl = getPersonDepartmentUrl(assignedUser); //null
			personAddress = getPersonAddress(assignedUser);
		}
		else
		{
			personID = getPersonId(caller); //null
			personName = getPersonName(caller); //null
			personImageUrl = getPersonImageUrl(caller); //null
			personUrl = getPersonUrl(caller); //null
			personEmail = getPersonEmail(caller); //null
			personPhones = getPersonPhones(caller);
			personJobTitle = getPersonJobTitle(caller);
			personManagerName = getPersonManagerName(caller);
			personManagerID = getPersonManagerID(caller);
			personManagerUrl = getPersonManagerUrl(caller); //null
			personLocation = getPersonLocation(caller); 
			personDepartment = getPersonDepartment(caller);
			personDepartmentID = getPersonDepartmentId(caller);
			personDepartmentUrl = getPersonDepartmentUrl(caller);
			personAddress = getPersonAddress(caller);
		}
		assignedUserName = getPersonName(assignedUser);
		workGroupName = getWorkGroupName();
		serviceName = getServiceName();
		problem = getProblem();
		workHistory = getWorkHistory();
		impact = getImpact();
		priority = getPriority();
		
		attachments = getAttachments(); //null
		approvalIniciatorName = getApprovalIniciatorName();
		approvalIniciatorID = getApprovalIniciatorID();
		approvalDescription = getApprovalDescription();
		approvalComment = getApprovalComment();
		approvalDeadline = getApprovalDeadline();
		approvalResult = getApprovalResult();
		approvalVotes = getApprovalVotes(); //null
		lastNote= getLastNote(); //null
		lastEditPerson = getLastEditPerson(); //null
		dataformScript = getDataformScript();
		Notifier.logger.trace("Разрываем сессию");
		//close session
		Session.disposeSession(session);
	}
	//---------------------------------------------------------------
	private void getProperties()
	{
		sPersonImageUrl = TrayManager.personImageUrl;
		sPersonUrl = TrayManager.personUrl;
		sPersonOrgUrl = TrayManager.personOrgUrl;
		sdWebUrl = TrayManager.sdWebUrl;
		dateFormat = TrayManager.dateFormat;
		if(dateFormat!=null)
		{
			try
			{
				sdf = new SimpleDateFormat(dateFormat);
			}catch(IllegalArgumentException iae)
			{
				Notifier.logger.warn("Неправильный формат даты");
			}
		}
	}
	// --------------------------------------------------------------
	private String getID()
	{
		return String.valueOf(serviceCall.getID());
	}
	private String getOID()
	{
		return String.valueOf(serviceCall.getOID());
	}
	private String getStatus()
	{
		if(serviceCall.getStatus()!=null)
			return serviceCall.getStatus().getText();
		else 
			return "";
	}
	private String getRegDate()
	{
		if(serviceCall.getRegistrationDate()!=null)
			return sdf.format(ApiDateUtils.double2Date(serviceCall.getRegistrationDate()));
		else
			return "";
	}
	private String getDescription()
	{
		return serviceCall.getDescription();
	}
	
	//--------------------------------------------
	private String getPersonName(IPerson person)
	{
		if(person != null)
			return person.getName();
		return null;
	}
	
	//--------------------------------------------
	private String getPersonEmail(IPerson person)
	{
		if(person == null)
			return null;
		String email = person.getEmailLowerCase();
		if(email!=null)
			return email.replace('\n', ' ').replace('\r', ' ').trim();
		return null;
	}
	
	//--------------------------------------------
	private String getPersonPhones(IPerson person)
	{
		if(person == null)
			return null;
		String personMDPhone = person.getPrimaryTelephoneNumber();
		String personPhone = person.getPersonText5();
		String phone="";
		if(personMDPhone != null && personMDPhone.length()==3)
			phone += personMDPhone;
		if(personPhone != null && personPhone.length() > 0)
		{
			if(phone.length() > 0)
				phone += ";"+personPhone;
			else 
				phone = personPhone;
		}
			
		return phone.replace('\n', ' ').replace('\r', ' ');
	}
	
	//--------------------------------------------
	private String getPersonJobTitle(IPerson person)
	{
		if(person == null)
			return null;
		return person.getPersonText1() != null ? person.getPersonText1() : "";
	}
	
	//--------------------------------------------
	private String getPersonManagerName(IPerson person)
	{
		IPerson personManager = getPersonManager(person);
		if(personManager != null)
		{
			return personManager.getName();
		}
		return "";
	}
	
	private String getPersonManagerID(IPerson person)
	{
		IPerson personManager = getPersonManager(person);
		if(personManager != null)
		{
			return getPersonId(personManager);
		}
		return "";
	}
	
	private String getPersonManagerUrl(IPerson person)
	{
		IPerson personManager = getPersonManager(person);
		if(personManager!=null)
		{
			return getPersonUrl(personManager);
		}
		return null;
	}
	
	//--------------------------------------------
	private String getPersonLocation(IPerson person)
	{
		if(person == null)
			return null;
		if(person.getLocation() != null)
			return person.getLocation().getRemark();
		return "";
	}
	
	private String getPersonDepartment(IPerson person)
	{
		if(person == null)
			return null;
		IOrganization department = person.getPersonOrganization();
		if(department!=null)
			return department.getName1();
		return "";
	}
	
	private String getPersonDepartmentId(IPerson person)
	{
		if(person == null)
			return null;
		IOrganization department = person.getPersonOrganization();
		String departmentId = getDepartmentId(department);
		if(departmentId != null)
			return departmentId;
		return "";
	}
	
	private String getPersonAddress(IPerson person)
	{
		if(person == null)
			return null;
		return person.getPersonText4() != null ? person.getPersonText4() : "";
	}
	
	private String getPersonId(IPerson person)
	{
		return person.getSourceID();
	}
	
	private String getPersonUrl(IPerson person)
	{
		if(sPersonUrl!=null && person!=null)
			return sPersonUrl.replace("[x]", getPersonId(person));
		return null;
	}
	
	private String getPersonImageUrl(IPerson person)
	{
		if(sPersonImageUrl!=null && person!=null)
			return sPersonImageUrl.replace("[x]", getPersonId(person));
		return null;
	}
	
	private static IPerson getPersonManager(IPerson person)
	{
		if (person == null)
			return null;
		// Get person organization
		IOrganization personOrganization = person.getPersonOrganization();
		if (personOrganization == null)
			return null;
		while (personOrganization.getOrganizationManager() == null
				|| personOrganization.getOrganizationManager().getSourceID().equals(person.getSourceID()))
		{
			personOrganization = personOrganization.getParentOrganization();
			if (personOrganization == null)
				return null;
		}
		return personOrganization.getOrganizationManager();
	}
	
	//--------------------------------------------
	private String getDepartmentId(IOrganization organization)
	{
		if(organization != null)
			return organization.getSourceID();
		return null;
	}
	
	private String getPersonDepartmentUrl(IPerson person)
	{
		if(person != null && person.getPersonOrganization() != null)
		{
			String personOrgID = getDepartmentId(person.getPersonOrganization());
			if(sPersonOrgUrl != null)
				return sPersonOrgUrl.replace("[x]", personOrgID);
		}
		
		return null;
	}
	
	//--------------------------------------------
	private String getWorkGroupName()
	{
		if(serviceCall.getAssignment().getAssignorWorkgroup()!=null)
			return serviceCall.getAssignment().getAssignorWorkgroup().getName();
		return "";
	}
	private String getServiceName()
	{
		if(serviceCall.getService()!=null)
			return serviceCall.getService().getName();
		return "";
	}
	private String getProblem()
	{
		if(serviceCall.getSer4k3()!=null)
			return serviceCall.getSer4k3().trim();
		return "";
	}
	private String getWorkHistory()
	{
		if(serviceCall.getServicecallText64kB()!=null)
			return serviceCall.getServicecallText64kB().trim();
		return "";
	}
	private String getImpact()
	{
		IImpact iImpact = serviceCall.getImpact();
		if (iImpact != null)
		{
			return iImpact.getText();
		}
		return "";
	}
	private String getPriority()
	{
		// Get priority
		IPriority iPriority = serviceCall.getPriority();
		if (iPriority != null)
		{
			return iPriority.getText();
		}
		return "";
	}
	
	//--------------------------------------------
	private Map<String, String> getAttachments()
	{
		// Get attachment
		IAttachment at = serviceCall.getAttachment();
		if (at.getAttachmentExists())
		{
			Map<String, String> mAttachments = new HashMap<String, String>();
			IAttachedItem[] attachments = serviceCall.getAttachment().getAttachedItems();
			for (IAttachedItem att:attachments)
        	{
        		String filename = att.getBaseName();
	        	String ext="";
	        	int lastDotIndex = filename.lastIndexOf('.');
	        	if(lastDotIndex!=-1 && lastDotIndex!=0)
	        		ext = filename.substring(lastDotIndex);
	        	
	        	String scoid = String.valueOf(serviceCall.getOID());
	        	String attoid = String.valueOf(att.getOID());
	        	
	        	String fileUrl = sdWebUrl+"/attach?fileExt="
	        		+ ext +"&scoid="+ scoid +"&attoid="+ attoid;
	        	mAttachments.put(filename, fileUrl);
        	}
			return mAttachments;
		}
		return null;
	}
	
	//--------------------------------------------
	private String getApprovalIniciatorName()
	{
		if(serviceCall.getApprovalInitiator()!=null)
		{
			return serviceCall.getApprovalInitiator().getName();
		}
		return "";
	}
	
	private String getApprovalIniciatorID()
	{
		if(serviceCall.getApprovalInitiator()!=null)
		{
			return getPersonId(serviceCall.getApprovalInitiator());
		}
		return "";
	}
	
	private String getApprovalDescription()
	{
		if(serviceCall.getApproval()!=null)
		{
			return serviceCall.getApproval().getDescription();
		}
		return "";
	}
	
	private String getApprovalComment()
	{
		return serviceCall.getApprove1() !=null ? serviceCall.getApprove1() : "";
		
	}
	
	private String getApprovalDeadline()
	{
		if(serviceCall.getApproval()!=null)
		{
			if(serviceCall.getApproval().getApprovalDeadline()!=null)
				return sdf.format(ApiDateUtils.double2Date(serviceCall.getApproval().getApprovalDeadline()));
		}
		return "";
	}
	
	private String getApprovalResult()
	{
		if(serviceCall.getApproval()!=null)
		{
			return serviceCall.getApproval().getApprovalResult();
		}
		return "";
	}
	
	private List<ApprovalVote> getApprovalVotes()
	{
		if(serviceCall.getApproval()!=null)
		{
			List<ApprovalVote> avList = new ArrayList<ApprovalVote>();
			for(IApprovalVote av:serviceCall.getApproval().getApprovalVotes())
			{
				ApprovalVote vote = new ApprovalVoteImpl(av);
				avList.add(vote);
			}
			return avList;
		}
		return null;
	}
	
	//--------------------------------------------
	private String getLastNote()
	{
		return serviceCall.getServicecallText18();
	}
	
	private String getLastEditPerson()
	{
		return serviceCall.getServicecallText1();
	}
	
	private String  getDataformScript()
	{
		return serviceCall.getServicecallText20();
	}
	
	/**-------------------------------------------
	 * Interface implementation
	 --------------------------------------------*/
	
	public String OID()
	{
		return OID;
	}
	
	public String ID()
	{
		return ID;
	}
	
	public String status()
	{
		return status;
	}
	
	public String regDate()
	{
		return regDate;
	}
	
	public String description()
	{
		return description;
	}
	
	public String assignedUserName()
	{
		return assignedUserName;
	}
	
	public String workGroupName()
	{
		
		return workGroupName;
	}
	
	public String serviceName()
	{
		
		return serviceName;
	}
	
	public String problem()
	{
		
		return problem;
	}
	
	public String workHistory()
	{
		
		return workHistory;
	}
	
	public String impact()
	{
		
		return impact;
	}
	
	public String priority()
	{
		
		return priority;
	}
	
	public String personID()
	{
		
		return personID;
	}
	
	public String personName()
	{
		
		return personName;
	}
	
	public String personImageUrl()
	{
		
		return personImageUrl;
	}
	
	public String personUrl()
	{
		
		return personUrl;
	}
	
	public String personEmail()
	{
		
		return personEmail;
	}
	
	public String personPhones()
	{
		
		return personPhones;
	}
	
	public String personJobTitle()
	{
		
		return personJobTitle;
	}
	
	public String personManagerName()
	{
		
		return personManagerName;
	}
	
	public String personManagerID()
	{
		
		return personManagerID;
	}
	
	public String personManagerUrl()
	{
		
		return personManagerUrl;
	}
	
	public String personLocation()
	{
		
		return personLocation;
	}
	
	public String personDepartment()
	{
		
		return personDepartment;
	}
	
	public String personDepartmentID()
	{
		
		return personDepartmentID;
	}
	
	public String personDepartmentUrl()
	{
		
		return personDepartmentUrl;
	}
	
	public String personAddress()
	{
		
		return personAddress;
	}
	
	public Map<String, String> attachments()
	{
		
		return attachments;
	}
	
	public String approvalIniciatorName()
	{
		
		return approvalIniciatorName;
	}
	
	public String approvalIniciatorID()
	{
		
		return approvalIniciatorID;
	}
	
	public String approvalDescription()
	{
		
		return approvalDescription;
	}
	
	public String approvalComment()
	{
		
		return approvalComment;
	}
	
	public String approvalDeadline()
	{
		
		return approvalDeadline;
	}
	
	public String approvalResult()
	{
		
		return approvalResult;
	}
	
	public List<ApprovalVote> approvalVotes()
	{
		
		return approvalVotes;
	}
	
	public String dataformScript()
	{
		
		return dataformScript;
	}
	
	public String lastNote()
	{
		
		return lastNote;
	}
	
	public String lastEditPerson()
	{
		
		return lastEditPerson;
	}
	
	public boolean isCaller()
	{
		
		return isCaller;
	}
	
	public boolean isAssigned()
	{
		
		return isAssigned;
	}
	
}

