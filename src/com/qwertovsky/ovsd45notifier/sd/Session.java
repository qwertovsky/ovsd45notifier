package com.qwertovsky.ovsd45notifier.sd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.hp.ifc.util.AppCryptor;
import com.hp.itsm.api.ApiSDSession;
import com.qwertovsky.ovsd45notifier.Notifier;
import com.sun.mail.util.BASE64DecoderStream;

public class Session
{
	private static List<ApiSDSession> sessions;
	private static String server = null;
	private static String username = null;
	private static String password = null;
	private static String client = "client2008";
	
	public static ApiSDSession getSession()
	{	
		//return exists or open new session
		if(sessions==null)
			sessions = new ArrayList<ApiSDSession>();
		
		//read properties
		try
		{
			FileInputStream propertiesFIS;
			propertiesFIS = new FileInputStream("conf/ovsd45notifier.properties");
			Properties prop = new Properties();
			prop.load(propertiesFIS);
			client = prop.getProperty("client");
			
		}
		catch(FileNotFoundException fnfe)
		{
			Notifier.logger.warn("Не найден файл ovsd45notifier.properties");
		}
		catch (IOException ioe)
		{
			Notifier.logger.warn("Невозможно прочитать файл ovsd45notifier.properties");
		}
		Notifier.logger.debug("Чтение логина, пароля и имя сервера Service Desk");
		//read login, password and server from user settings file 
		try
		{
			String userSettingsFile;
			if("client".equalsIgnoreCase(client))
				userSettingsFile = System.getProperty("user.home")
					+"\\Application Data\\Hewlett-Packard\\OpenView\\Service Desk\\user_settings.xml";
			else
				userSettingsFile = System.getProperty("user.home")
					+"\\Application Data\\Hewlett-Packard\\OpenView\\Service Desk\\Client 2008\\data\\user_settings.xml";
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(userSettingsFile);
			//settings
			Element settingsElement = doc.getDocumentElement();
			
			//groups
			NodeList rootGroups = settingsElement.getElementsByTagName("GROUP");
			//accounts group
			Element accountsGroup = null;
			for(int i=0; i<rootGroups.getLength();i++)
			{
				if(((Element)rootGroups.item(i)).getAttribute("NAME").equals("ACCOUNTS"))
				{
					accountsGroup = (Element) rootGroups.item(i);
					break;
				}
			}
			if(accountsGroup==null)
				throw new Exception();
			//get default account group name
			NodeList childPropertiesAccountsGroup = accountsGroup.getElementsByTagName("PROPERTY");
			String defaultGroupName = "1";
			for(int i=0;i<childPropertiesAccountsGroup.getLength();i++)
			{
				if(((Element)childPropertiesAccountsGroup.item(i)).getAttribute("NAME").equals("DEFAULT"))
				{
					defaultGroupName = (childPropertiesAccountsGroup.item(i)).getTextContent();
					break;
				}
			}
			//get default account group
			NodeList accountGroups = accountsGroup.getElementsByTagName("GROUP");
			Element accountGroup = null;
			for(int i=0; i<accountGroups.getLength();i++)
			{
				if(((Element)accountGroups.item(i)).getAttribute("NAME").equals(defaultGroupName))
				{
					accountGroup = (Element) accountGroups.item(i);
					break;
				}
			}
			if(accountGroup==null)
				throw new Exception();
			//get properties
			String cryptPassword = null;
			NodeList propertiesNL = accountGroup.getElementsByTagName("PROPERTY");
			for(int i=0;i<propertiesNL.getLength();i++)
			{
				if(((Element)propertiesNL.item(i)).getAttribute("NAME").equals("SD ACCOUNT NAME"))
				{
					username = propertiesNL.item(i).getTextContent();
				}
				if(((Element)propertiesNL.item(i)).getAttribute("NAME").equals("SD PASSWORD"))
				{
					cryptPassword = propertiesNL.item(i).getTextContent();
				}
				if(((Element)propertiesNL.item(i)).getAttribute("NAME").equals("SD SERVER"))
				{
					server = propertiesNL.item(i).getTextContent();
				}
			}
			if(username==null || cryptPassword==null || server==null)
				throw new Exception();
			cryptPassword = new String(BASE64DecoderStream.decode(cryptPassword.getBytes()), "utf-16");
			password = new String(AppCryptor.transform("sdk39df#@4!7-sd", BASE64DecoderStream.decode(cryptPassword.getBytes()) ), "utf-16");
		}catch(Exception e)
		{
			Notifier.logger.error("Не настроен клинт. Возможно вы ни разу не запускали его.");
			System.exit(-1);
		}
		
		ApiSDSession session;
		Notifier.logger.debug("Соединение с сервером Service Desk...");
		/*
		 * Open a session to a running Service Desk application Server. Once you
		 * have a connection, you can use it to communicate with the server. The
		 * connection is to an instance of the workflow layer in the server that
		 * keeps state information for this client.
		 */
		try
		{
			session = ApiSDSession.openSession(server, username, password);
			Notifier.logger.debug("Установлено соединение с Service Desk");
		} catch (Exception e)
		{
			/*
			 * Connecting can go wrong for various reasons. E.G. No server is
			 * running on this particular computer/port combination or the
			 * user/password combination was wrong. Catch the exception and
			 * print an error message for the user or for the log. The Web-API
			 * makes an effort to give sensible messages in the exceptions that
			 * it throws, and if possible, the messages are localised.
			 * 
			 */
			if(e.getMessage().startsWith("java.net.UnknownHostException"))
				Notifier.logger.error("Неизвестный сервер Service Desk или отсутствует сетевое подключение");
        	else
        		e.printStackTrace();
			return null;
		}
		sessions.add(session);
		return session;
	}
	//----------------------------------------------
	public static void disposeSession(ApiSDSession session)
	{
		//dispose session
		if(sessions==null)
			return;
		if(sessions.isEmpty())
			return;
		sessions.remove(session);
		session.closeConnection();
		session = null;
	}
	
	public static String getServer()
	{
		return server;
	}
}

