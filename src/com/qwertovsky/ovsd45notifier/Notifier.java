package com.qwertovsky.ovsd45notifier;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.qwertovsky.ovsd45notifier.monitors.EmailMonitor;
import com.qwertovsky.ovsd45notifier.monitors.ViewMonitor;

public class Notifier
{
	public static Logger logger = Logger.getLogger("com.qwertovsky.ovsd45notifier");
	private static EmailMonitor eMonitor = null;
	private static ViewMonitor vMonitor = null;
	private static Display display;

	static class Log4jStream extends PrintStream
	{
		boolean err = false;
		public Log4jStream(OutputStream out)
		{
			super(out);
			if(out.equals(System.err))
				err = true;
		}

		@Override
		public void print(String string)
		{
			if(err)
				logger.error(string);
			else logger.info(string);
		}
		@Override
		public  void print(Object object)
		{
			print(object.toString());
		}
	}

	//---------------------------------------------------
	public static void main(String args[])
	{
		
		//set thread name
		Thread.currentThread().setName("Notifier");
		//set logger configuration file 
		DOMConfigurator.configureAndWatch("./conf/log4j.xml");
		System.setOut(new Log4jStream(System.out));
		System.setErr(new Log4jStream(System.err));
		
		
		logger.info("Старт программы");
		InputStream manifestStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("META-INF/MANIFEST.MF");
		try
		{
			Manifest manifest = new Manifest(manifestStream);
			Attributes attributes = manifest.getMainAttributes();
			String impVersion = attributes.getValue("Implementation-Version");
			String builtDate = attributes.getValue("Built-Date");
			logger.info("Built date: " + builtDate +", Version: "+impVersion);
		} catch (IOException ex)
		{
			logger.warn("Error while reading version: " + ex.getMessage());
		}

		logger.trace("Display & Shell");		
		display = new Display();
		Shell shell = new Shell(display, SWT.NO_FOCUS);
		logger.trace("TrayManager");
		TrayManager trayManager = new TrayManager();
		trayManager.initialize(shell);
		
		logger.trace("Создаем мониторы");
		//run email and view monitors
		if(TrayManager.bEmailMonitor)
			startEmailMonitor();
		if(TrayManager.bViewMonitor)
			startViewMonitor();
		
		if (((eMonitor == null) || (!eMonitor.isAlive())) && (
			      (vMonitor == null) || (!vMonitor.isAlive())))
	    {
			display.dispose();  
			logger.info("Ни один из мониторов не запущен");
			if(!TrayManager.bEmailMonitor && !TrayManager.bViewMonitor)
				logger.info("В настройках не указан монитор");
			logger.info("Программа остановлена\r\n" +
		      		"-----------------------------------------------------------------");
		     return;
	    }
		logger.info("Программа запущена");
		
		while (!shell.isDisposed())
		{
			try
			{
				if (!display.readAndDispatch())
				{
					display.sleep();
				}
			} catch (Throwable e)
			{
				System.out.println("throwable");
				e.printStackTrace();
			}
		}
		
		logger.debug("Программа останавливается...");
		if(eMonitor!=null)
			eMonitor.interrupt();
		if(vMonitor!=null)
			vMonitor.interrupt();
		try
		{
			if(vMonitor!=null)
				vMonitor.join();
		} catch (InterruptedException e)
		{
			
		}
		try
		{
			if(eMonitor!=null)
				eMonitor.join();
		} catch (InterruptedException e)
		{
			
		}
		

		display.dispose();
        logger.info("Программа остановлена\r\n" +
        		"-----------------------------------------------------------------");
        
	}
	//-------------------------------------------------------------
	protected static void startEmailMonitor()
	{
		try
		{
			eMonitor = new EmailMonitor();
			eMonitor.setName("EmailMonitor");
			eMonitor.start();
		} catch (Exception e1)
		{
			logger.warn("Работа с почтой не запущена");
		}
		
	}
	//-------------------------------------------------------------
	protected static void startViewMonitor()
	{
		try
		{
			vMonitor = new ViewMonitor();
			vMonitor.setName("View Monitor");
			vMonitor.start();
		} catch (Exception e1)
		{
			logger.warn("Монитор проверки вида не запущен");
		}
		
	}
	//-------------------------------------------------------------
	protected static void stopEmailMonitor()
	{
		if(eMonitor != null && eMonitor.isAlive())
		{
			eMonitor.interrupt();
		}
		
	}
	//-------------------------------------------------------------
	protected static void stopViewMonitor()
	{
		if(vMonitor != null && vMonitor.isAlive())
		{
			vMonitor.interrupt();
			
		}
		
	}
}

