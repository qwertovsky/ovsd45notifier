package com.qwertovsky.ovsd45notifier.sd;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import com.hp.itsm.api.ApiSDSession;

public class SessionTest
{

	@Test
	public void testGetSession()
	{
		ApiSDSession session = Session.getSession();
		if(session==null)
			fail("no session");
		Session.disposeSession(session);
	}

	@Test
	@Ignore
	public void testDisposeSession()
	{
		fail("Not yet implemented"); // TODO
	}

}
