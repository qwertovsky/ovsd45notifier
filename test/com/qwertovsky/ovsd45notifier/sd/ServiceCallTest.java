package com.qwertovsky.ovsd45notifier.sd;

import static org.junit.Assert.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import com.qwertovsky.ovsd45notifier.TrayManager;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;

public class ServiceCallTest
{

	@Test
	public void testServiceCall()
	{
		Shell shell = new Shell(new Display(), SWT.NO_FOCUS);
		TrayManager trayManager = new TrayManager();
		trayManager.initialize(shell);
		ServiceCall sc = new ServiceCallImpl(1014024);
		
		assertEquals("Bad caller","183033",sc.personID());
		assertEquals("Bad department","25391",sc.personDepartmentID());
	}

}
