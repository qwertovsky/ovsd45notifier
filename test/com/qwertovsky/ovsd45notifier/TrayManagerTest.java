package com.qwertovsky.ovsd45notifier;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;
import com.qwertovsky.ovsd45notifier.sd.ServiceCallImpl;

public class TrayManagerTest
{

	@Test
	public void testFlash()
	{
		Display display = new Display();
		Shell shell = new Shell(display, SWT.NO_FOCUS);
		
		TrayManager trayManager = new TrayManager();
		trayManager.initialize(shell);
		List<ServiceCall> list = new ArrayList<ServiceCall>();
		list.add(new ServiceCallImpl(669856));
		TrayManager.flash(list);
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
		display.dispose();
	}

}
