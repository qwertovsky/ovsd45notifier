package com.qwertovsky.ovsd45notifier.mail;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.junit.Test;


public class MessageRemovedTest
{
	@Test
	public void removeMessage() throws IOException, MessagingException
	{
		Properties prop = new Properties();
		FileInputStream propertiesFIS = new FileInputStream(
				"conf/ovsd45notifier.properties");
		prop.load(propertiesFIS);

		String imapHostName = prop.getProperty("imapHostName");
		String imapPort = prop.getProperty("imapPort");
		String user = prop.getProperty("user");
		String password = prop.getProperty("password");

		Properties mailProp = new Properties();
		mailProp.put("mail.imap.host", imapHostName);
		mailProp.put("mail.imap.port", imapPort);
		mailProp.put("mail.mime.charset", "utf-8");

		Session session = Session.getDefaultInstance(mailProp);
		Store store = session.getStore("imap");
		store.connect(user, password);
		Folder inbox = store.getFolder("INBOX");
		inbox.open(Folder.READ_WRITE);
		Message[] messages = inbox.getMessages(1, 3);
		for(Message m: messages)
		{
			m.setFlag(Flag.DELETED, true);
			
		}
		inbox.expunge();
		inbox.close(true);
		store.close();
		
	}
}
