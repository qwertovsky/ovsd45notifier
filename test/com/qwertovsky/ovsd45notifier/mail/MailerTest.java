package com.qwertovsky.ovsd45notifier.mail;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
//import org.junit.Ignore;
import org.junit.Test;

import com.qwertovsky.ovsd45notifier.TrayManager;
import com.qwertovsky.ovsd45notifier.plugin.ServiceCall;
import com.qwertovsky.ovsd45notifier.sd.ServiceCallImpl;

public class MailerTest
{

	
	//------------------------------------------------------
	@Test(timeout=30000)
//	@Ignore
	public void testIMAPGetMail() throws Exception
	{
		// get properties
		Properties prop = new Properties();
		FileInputStream propertiesFIS = new FileInputStream(
				"conf/ovsd45notifier.properties");
		prop.load(propertiesFIS);

		String imapHostName = prop.getProperty("imapHostName");
		String imapPort = prop.getProperty("imapPort");
		String user = prop.getProperty("user");
		String password = prop.getProperty("password");
		String emailFrom = prop.getProperty("emailFrom");
		String emailTo = prop.getProperty("emailTo");

		Properties mailProp = new Properties();
		mailProp.put("mail.imap.host", imapHostName);
		mailProp.put("mail.imap.port", imapPort);
		mailProp.put("mail.mime.charset", "utf-8");

		Session session = Session.getDefaultInstance(mailProp);
		Store store = session.getStore("imap");
		store.connect(user, password);
		Folder inbox = store.getFolder("INBOX");
		inbox.open(Folder.READ_WRITE);
		try
		{
			IMAPMailer mailer = new IMAPMailer();
			MimeMessage m = new MimeMessage(session);
			m.setContent("test", "text/plain; charset=utf-8");
			m.setSubject("На Вашу группу назначена Заявка № 676750.");
			m.setFrom(new InternetAddress(emailFrom, "Service Desk"));
			m.setRecipient(RecipientType.TO, new InternetAddress(emailTo));
			inbox.appendMessages(new Message[] { m });
			// m.setFlag(Flag.SEEN, true);
			Map<String, String> subjects = new HashMap<String,String>();
			while(true)
			{
				Thread.sleep(5000L);
				subjects = mailer.getMail();
				if (!subjects.isEmpty())
					break;
			}
			
			
			Set<Entry<String, String>> ss = subjects.entrySet();
			for (Entry<String, String> s : ss)
			{
				System.out.println(s.getValue());
			}
		} catch (AddressException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	//----------------------------------------------
	@Test
//	@Ignore
	public void testPOPGetMail() throws FileNotFoundException, IOException,
			NoSuchProviderException, MessagingException, InterruptedException
	{
		Properties prop = new Properties();
		FileInputStream propertiesFIS = new FileInputStream(
				"conf/ovsd45notifier.properties");
		prop.load(propertiesFIS);

		String smtpHostName = prop.getProperty("smtpHostName");
		String smtpPort = prop.getProperty("smtpPort");
		String emailFrom = prop.getProperty("emailFrom");
		String emailTo = prop.getProperty("emailTo");

		Properties mailProp = new Properties();
		mailProp.put("mail.smtp.host", smtpHostName);
		mailProp.put("mail.smtp.port", smtpPort);
		mailProp.put("mail.mime.charset", "utf-8");

		Session session = Session.getDefaultInstance(mailProp);
		MimeMessage m = new MimeMessage(session);
		m.setContent("test", "text/plain; charset=utf-8");
		m.setSubject("На Вашу группу назначена Заявка № 676750.");
		m.setFrom(new InternetAddress(emailFrom, "Service Desk"));
		m.setRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
		Transport.send(m);

		POPMailer mailer = new POPMailer();
		Thread.sleep(10000L);
		Map<String, String> subjects = mailer.getMail();
		if (subjects.isEmpty())
		{
			fail("no message");
		}
		Set<Entry<String, String>> ss = subjects.entrySet();
		for (Entry<String, String> s : ss)
		{
			System.out.println(s.getValue());
		}
	}
	//-----------------------------------------------
	@Test
//	@Ignore
	public void testPOPSend() throws FileNotFoundException, IOException
	{
		TrayManager tm = new TrayManager();
		tm.initialize(new Shell(Display.getDefault()));
		ServiceCall sc = new ServiceCallImpl(676750L);
		POPMailer mailer = new POPMailer();
		mailer.send(sc, "test 676750");
	}

	// ----------------------------------------------
	@Test
//	@Ignore
	public void testIMAPSend() throws NoSuchProviderException,
			MessagingException, Exception
	{
		TrayManager tm = new TrayManager();
		tm.initialize(new Shell(Display.getDefault()));
		ServiceCall sc = new ServiceCallImpl(676750L);
		IMAPMailer mailer = new IMAPMailer();
		mailer.send(sc, "test 676750");
	}
}
